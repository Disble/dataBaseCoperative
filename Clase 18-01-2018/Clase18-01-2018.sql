create table Proveedor(
	codigo int,
	nombre varchar(50),
	ciudad varchar(50)
)

insert into Proveedor values (1,'Luis','Quito')
insert into Proveedor values (2,'Pedro','Guayaquil')
insert into Proveedor values (3,'Maria','Ambato')

drop table Proveedor

select * from Proveedor

--1. Clave primaria en c�digo,controlar que cada codigo inicie con p- y luego un numero de 3 digitos
	--Ejemplo: p-001

	alter table Proveedor alter column codigo char(5) not null

	update Proveedor set codigo='p-001' where codigo='1'
    update Proveedor set codigo='p-002' where codigo='2'
    update Proveedor set codigo='p-003' where codigo='3'
	
    alter table Proveedor add constraint chk_codigo_Proveedor check (codigo like '%(^p-)[0-9]{3}%')

	insert into Proveedor values (1,'Esteban','Otavalo')
	exec sp_helpconstraint 'Proveedor'


	alter table Proveedor add constraint pk_codigoProv_Proveedor
	primary key clustered (codigo)
	
--2. Crear un default 'IBARRA'sobre la columna ciudad.
--3. Controlar que los nombres de los proveedores contengan solo letras.
--4. Crear integridad referencial entre Proveedores y Facturas.