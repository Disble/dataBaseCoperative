-- CREATE DATABASE Facturacion

USE Facturacion
GO

-- Crear la tabla PROVEEDOR(codigo, nombre, ciudad) sin restricciones.
-- Ingresar tres filas, luego, sin borrar la tablas crear las siguientes restricciones:

CREATE TABLE PROVEEDOR(
	codigo int,
	nombre varchar(20),
	ciudad varchar(20)
);

INSERT INTO PROVEEDOR VALUES(1, 'Juan', 'Ibarra')
INSERT INTO PROVEEDOR VALUES(1, 'Diana', 'Quito')
INSERT INTO PROVEEDOR VALUES(3, 'Ivana', 'Riobamaba')

-- 1. Clave primaria en c�digo, controlar que cada codigo inicie 
-- con P- y luego un n�mero de tres digitos. Ejemplo: 'P-001'

select * from PROVEEDOR

alter table PROVEEDOR alter column codigo CHAR(5) not null 

update PROVEEDOR set codigo = 2 where nombre='Diana'

alter table PROVEEDOR add constraint pk_codigoPro_Proovedor
primary key clustered (codigo)

update PROVEEDOR set codigo='P-001' where codigo='1'
update PROVEEDOR set codigo='P-002' where codigo='2'
update PROVEEDOR set codigo='P-003' where codigo='3'

	-- Alter table PROVEEDOR Drop constraint chk_codigoPro_Proovedor

alter table PROVEEDOR add constraint chk_codigoPro_Proovedor
CHECK (left(codigo, 2)='P-' AND isnumeric(right(codigo, 3))=1 AND LEN(codigo)=5)

INSERT INTO PROVEEDOR VALUES('P-004', 'Xia', 'Riobamaba')

-- 2. Crear un Default 'IBARRA' sobre la columna ciudad

ALTER TABLE PROVEEDOR ADD CONSTRAINT df_ciudad_Proovedor
DEFAULT 'IBARRA' FOR ciudad

INSERT INTO PROVEEDOR(codigo, nombre) VALUES('P-005', 'Alma')

-- 3. Comprobar que los nombres de los proveedores contengan solo letras

alter table PROVEEDOR add constraint chk_nombre_Proovedor
CHECK (nombre not like '%[^A-Z]%')

	-- Alter table PROVEEDOR Drop constraint chk_nombre_Proovedor

INSERT INTO PROVEEDOR(codigo, nombre) VALUES('P-006', 'Hilda')

-- 4. Crear integridad referencial entre Proveedores y Facturas. 
-- Un Proveedor varias Facturas

alter table Facturas add codigoPro char(5)

alter table Facturas add constraint fk_Proveedores_Facturas
foreign key (codigoPro) references Proveedor(codigo)

select * from Facturas

	-- update Facturas set codigoPro='P-010' WHERE codFactura=1

update Facturas set codigoPro='P-006' WHERE codFactura=1