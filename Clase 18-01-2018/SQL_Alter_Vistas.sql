--LA INSTRUCCI�N ALTER
--Esta instrucci�n se utiliza para modificar esquemas de bases de datos o modificar objetos ya creados.
--Es una de las instrucciones del Lenguaje de Definici�n de Datos (DDL). Otras instrucciones de DDL son
--CREATE, para crear esquemas y objetos, y DROP, para borrar esquemas u objetos.

drop table Empleados

create table Empleados
(
	codigoEmp int,
	nombreEmp varchar(30),
	cargoEmp varchar(30),
	sueldoEmp money,
	fechaIngreso date
)

insert into Empleados values (1,'Ana','Secretaria',500,'01-06-2014')
insert into Empleados values (1,'Pedro', 'Conserje',400,'03-11-2014')
insert into Empleados values (3,'Luis', 'Conserje',400,'21-03-2015')
insert into Empleados values (null,'Ana','Gerente',1500,'01-06-2014')

select * from Empleados
--Crear una restrici�n clave primaria en codigoemp sin borrar la tabla
alter table Empleados add constraint pk_codigoEmp_Empleados
primary key clustered (codigoEmp)

--Borrar las filas que tienen null en codigoEmp
delete from Empleados where codigoEmp is null

--Primero debemos poner en codigoEmp la restricci�n NOT NULL
alter table Empleados alter column codigoEmp int not null

--Debemos modificar el dato duplicado en codigoEmp
update Empleados set codigoEmp = 2 where nombreEmp='Pedro'
--Ahora si ejecutamos la instrucci�n que crea la clave primaria

select * from Empleados

insert into Empleados values (4,'Ana','Gerente',1000,'12-06-2015')

--Probemos que ya no se pueden insertar nivalores nulos ni duplicados en codigoEmp
insert into Empleados values(null, 'Andr�s', 'T�cnico', 800, getdate())
insert into Empleados values(4, 'Andr�s', 'T�cnico', 800, getdate())

--CREAR UNA RESTRICCI�N UNIQUE SOBRE LA COLUMNA NOMBREEMP
alter table Empleados add constraint u_nombreEmp_Empleados
unique nonclustered (nombreEmp)

--Primero debemos corregir el nombre que est� duplicado
update Empleados set nombreEmp='Mar�a' where codigoEmp=4

--Ahora si podemos ejecutar la instrucci�n que crea la restricci�n UNIQUE

insert into Empleados values (5,'Ana','Gerente',1000,'12-06-2015')

--CREAR UNA RESTRICCI�N DEFAULT en CargoEmp
alter table Empleados add constraint df_cargoEmp_Empleados
default 'Conserje' for cargoEmp

insert into Empleados(codigoEmp,nombreEmp) values (6,'Juan')

--AGREGAR UNA COLUMNA codigoFact en la tabla Empleados
alter table Empleados add codigoFact int

--borrar la columna codigoFact de la tabla Empleados
alter table Empleados drop column codigoFact

select * from Empleados

--CREAR LA COLUMNA codigoEmp en la tabla Facturas
alter table Facturas add codigoEmp int

select * from Facturas
select * from Empleados
--Como no hay integridad referencial entre Empleados y Facturas, se puede ingresar cualquier codigo
--de empleado en Facturas, as� no exista ese c�digo en Empleados. Ejemplo:
insert into Facturas values(21,4,0,'02-02-2016',10)

--CREAR INTEGRIDAD REFERENCIAL ENTRE EMPLEADOS Y FACTURAS
alter table Facturas add constraint fk_Empleados_Facturas
foreign key (codigoEmp) references Empleados(codigoEmp)

--Primero se debe poner un valor v�lido de codigoEmp en vez de 10
update Facturas set codigoEmp=1 where codigoEmp=10

--Ahora se puede crear la integridad referencial
insert into Facturas values(22,2,0,'02-02-2016',10)-- No acepta esta fila
insert into Facturas values(22,2,0,'02-02-2016',4)

--CREAR UNA RESTRICCI�N CHECK para controlar que en cargo solo se inserten valores gerente, secretaria
--o conserje
alter table Empleados add constraint ck_cargoEmp_Empleado
check (cargoEmp in ('gerente','secretaria','conserje'))

insert into Empleados values(7,'Miguel','T�cnico',600,'30-01-2014')

select * from Empleados
select * from Facturas

--PARA VERIFICAR LAS RESTRICCIONES QUE EST�N TRABAJANDO EN UNA TABLA
exec sp_helpconstraint 'Empleados'

--EJERCICIO: crear la tabla PROVEEDOR(codigo, nombre,ciudad) sin restricciones. Ingresar 3 filas,
--luego, sin borrar la tabla crear las siguientes restricciones:
--1. Clave primaria en c�digo
--2. Crear un default 'IBARRA'sobre la columna ciudad.
--3. Controlar que los nombres de los proveedores contengan solo letras.
--4. Crear integridad referencial entre Proveedores y Productos.

alter table Proveedor check (nombre not like '%[^A-Z]%')

--VISTAS (VIEW)
--Son tablas virtuales, que no existen realmente en un esquema pero que se crean a partir de las tablas
--verdaderas como una forma de enmascarar u ocultar la verdadera forma de las tablas. Una vez creadas
--las vistas pueden utilizarse como cualquier tabla.
--CREAR UNA VISTA CON LA CONSULTA: Desplegar el c�digo y nombre de cada cliente junto con sus facturas 

create view v_FacturasPorCliente
as
select c.codigoCliente, nombreCliente, apellidoCliente, codigoFactura, valorFactura, fechaFactura 
from Clientes c inner join Facturas f
on c.codigoCliente=f.codigoCliente


--Una vez creada la vista se puede trabajar con ella como si fuera una tabla
select * from v_FacturasPorCliente

insert into v_FacturasPorCliente values(21,'Carlos','Rosales',3,0,getdate())
--No se puede insertar filas en vistas que est�n formadas desde varias tablas base

select * from Clientes
select * from Facturas

insert into Facturas values(3,97,0,getdate(),1)
--Las vistas se actualizan automaticamente con las inserciones de las tablas base

--Crear una vista a partir de una sola tabla
create view v_NombreProducto
as
select codigoProducto, nombreProducto from Productos

select * from v_NombreProducto

insert into v_NombreProducto values('Caf�')--Acepta la inserci�n a trav�s de la vista porque est� 
--creada desde una sola tabla base
select * from Productos

delete from v_NombreProducto where codigoProducto=1107

alter table empleados drop constraint ck_cargoEmp_Empleado


--CREAR UNA TABLA CON RESTRICCIONES DESDE SU CREACI�N

--Se quiere eliminar la tabla Empleados.
drop table Empleados
--Primero se debe borrar la integridad referencial con la tabla FACTURAS
EXEC SP_HELPCONSTRAINT 'FACTURAS'

select * from Facturas

--Borrar la integridad referencial entre Empleados y Facturas
Alter table Facturas Drop constraint fk_Empleados_Facturas

select * from Empleados

--Borrar la tabla Empleados
drop table Empleados

--Vamos a crear nuevamente a la tabla Empleados, con las restricciones necesarias.
create table Empleados
(
	codigoEmp int primary key,
	nombreEmp varchar(50) unique,
	cargo varchar(50) not null check(cargo in('conserje','secretaria','gerente')) default 'conserje',
	sueldo money check(sueldo>366),
	fechaIngreso date check(datename(dw,fechaIngreso) not in('s�bado','domingo'))
)

alter table Empleados add constraint df_fechaIngreso default getdate() for fechaIngreso

insert into Empleados values(1,'Julio',default,400,'26-06-2017')
insert into Empleados(codigoEmp,nombreEmp,cargo,sueldo) values(2,'Ana','gerente',1400)
insert into Empleados(codigoEmp,nombreEmp, cargo) values(3,'Marta','secretaria')

select * from Empleados
select * from Facturas

--Al no estar relacionadas las tablas Facturas y Empleados, se puede ingresar en Facturas codigos
--de empleados que no existan en la tabla Empleados


update Facturas set codigoEmp=3 where codigoEmp=4


alter table Facturas add constraint fk_Empleados_Facturas
foreign key (codigoEmp) references Empleados(codigoEmp)

