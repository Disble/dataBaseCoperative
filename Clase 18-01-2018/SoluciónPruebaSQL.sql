--BASE DE DATOS 1
--SOLUCION A LA PRUEBA DE SQL 
 
 drop database Facturacion
 go
create database Facturacion
go
use Facturacion
go

CREATE TABLE CLIENTES (
codigoCLi char(3) PRIMARY KEY,
nombreCli varchar(100),
apellidoCli varchar(100),
CiudadCli varchar(100)
)
go


CREATE TABLE FACTURAS (
codigoFact char(3) PRIMARY KEY,
codigoCli char(3),
valorFact money default 0,
fechaFact date,
CodigoEmpleado int,
FOREIGN KEY(codigoCLi) REFERENCES CLIENTES (codigoCLi)
)
go

CREATE TABLE PRODUCTOS (
codigoProd char(3) PRIMARY KEY,
nombreProd varchar(100),
unidades varchar(50),
cantidadStock dec(8,2),
precioUnitarioActual money
)
go


CREATE TABLE DETALLES (
codigoFact char(3) not null,
codigoProd char(3) not null,
cantidadComprada dec(8,2),
precioUnitarioCompra money,
FOREIGN KEY(codigoFact) REFERENCES FACTURAS (codigoFact)
)
go
ALTER TABLE DETALLES ADD PRIMARY KEY(codigoFact,codigoProd)
ALTER TABLE DETALLES ADD FOREIGN KEY(codigoProd) REFERENCES PRODUCTOS (codigoProd)
go

--GRUPO 1
--1.	Crear la tabla Empleados con los siguientes atributos y restricciones:
--	a.	CodigoEmpleado entero autoincrementable que inicia en 200 e incrementa de 1 en 1.
--	b.	NombreEmpleado no debe ser nulo, debe tener m�nimo 5 caracteres.
--	c.	ApellidoEmpleado no debe ser nulo, debe terminar con vocal.
--	d.	fechaIngreso debe ser mayor o igual al 12 de junio de 2010, por defecto la fecha actual.
--	e.	SueldoEmpleado, debe estar entre 400 y 800 d�lares, adem�s, cada empleado debe tener diferente sueldo.
--	f.	La tabla Empleados debe estar relacionada con la tabla Facturas, de tal manera que un empleado se relacione con una o m�s facturas.
--	g.	Ingresar al menos 3 empleados, todos relacionados con Facturas.

create table EMPLEADO(
codEmpleado int primary key identity(200,1),
nombreEmpleado varchar(20) not null  check(len(nombreEmpleado)>=5),
apellidoEmpleado varchar(20) not null check(apellidoEmpleado like '%[AEIOU]'),
fechaIngreso date default getdate()  check(fechaIngreso>'06/12/2010'),
SueldoEmpleado money check(SueldoEmpleado>=400 and SueldoEmpleado<=800) unique
)

alter table FACTURAS add codEmpleado int REFERENCES EMPLEADO

--Consultas
--2.	Desplegar el c�digo de cada factura junto al n�mero de filas de detalle que tiene cada una.
select f.codigoFact, count(*) from FACTURAS f inner join DETALLES d on f.codigoFact=d.codigoFact 
group by f.codigoFact

--3.	Desplegar el nombre de los productos que no se han vendido.
select nombreProd from PRODUCTOS p left outer join DETALLES d on p.codigoProd=d.codigoProd 
where d.codigoProd is null

--4.	Desplegar el nombre del producto m�s vendido.
select d.codigoProd, p.nombreProd,count(*) AS 'Nro de Ventas' into #Temporal1 from DETALLES d , Productos p 
where d.codigoProd=p.codigoProd group by d.codigoProd,p.nombreProd

select nombreProd from #Temporal1 where [Nro de Ventas]=(select max([Nro de Ventas]) from #Temporal1)

--5.	Desplegar los datos del empleado m�s antiguo.
select * from EMPLEADOS where fechaIngreso=(select min(fechaIngreso) from EMPLEADOS)

--6.	Despegar el nombre y apellido del cliente que m�s dinero a gastado en compras.
select nombreCli, apellidoCli, sum(valorFact) as 'Valor Pagado' into #Temporal2 
from CLIENTES c inner join FACTURAS f on c.codigoCLi=f.codigoCLi group by nombreCli, apellidoCli
 
select nombreCli, apellidoCli from #Temporal2 where [Valor Pagado]=(select max([Valor Pagado]) from #Temporal2)



--GRUPO 2
--1.	Crear la tabla Empleados con los siguientes atributos y restricciones:
--	a.	CodigoEmpleado entero autoincrementable que inicia en 100 e incrementa de 2 en 2.
--	b.	NombreEmpleado puede ser nulo y no debe comenzar con vocal.
--	c.	ApellidoEmpleado no debe ser nulo, debe tener m�ximo 5 caracteres.
--	d.	fechaIngreso debe estar entre enero y marzo de cualquier a�o.
--	e.	SueldoEmpleado, debe ser menor que 800 d�lares, por defecto debe ser 386.
--	f.	La tabla Empleados debe estar relacionada con la tabla Facturas, de tal manera que un empleado se relacione con una o m�s facturas.
--	g.	Ingresar al menos 3 empleados, todos relacionados con Facturas.

CREATE Table Empleados (
CodigoEmpleado int primary key identity(100,2),
NombreEmpleado  VARCHAR(50) check(NombreEmpleado not like '[AEIOU]%'), 
ApellidoEmpleado  VARCHAR(50) not null check(len(ApellidoEmpleado)<=5), 
fechaIngreso date check(datepart (mm,fechaIngreso)>=1 and datepart (mm,fechaIngreso)<=3),
SueldoEmpleado money default 386 check(SueldoEmpleado<800),
)
go

ALTER TABLE FACTURAS ADD FOREIGN KEY(codigoEmpleado) REFERENCES EMPLEADOS (codigoEmpleado)
go

---Ingreso Clientes--------
select*from CLIENTES
insert into CLIENTES values(1,'Ana','Morales',NULL)
insert into CLIENTES values(2,'Carlos','Puente','Otavalo')
insert into CLIENTES values(3,'Maria',	'Paredes',	'Ibarra')
insert into CLIENTES values(4,'Juan',	'Torres',	'Ibarra')
insert into CLIENTES values(6 ,'Cecilia',	'Torres',	NULL)
insert into CLIENTES values(7 ,'Cecilia',	'Enriquez',	NULL)
insert into CLIENTES values(8	,'Ana'	,'Naranjo'	,'Quito')
insert into CLIENTES values(9	,'Jose',	'Paez',	'Quito')
insert into CLIENTES values(20	,'Abdala'	,'Bucaram',	'Panam�')
insert into CLIENTES values(21	,'Rafael'	,'Correa',	'Bruselas')
insert into CLIENTES values(95	,'Marta'	,'Nu�ez',	'Ibarra')
insert into CLIENTES values(97	,'Polo'	,'Vaca',	'Quito')
insert into CLIENTES values(98	,'Angelica',	'Espinosa',	'Ibarra')

------Ingres de Productos--------
select*from PRODUCTOS
INSERT INTO PRODUCTOS VALUES(100, 'Arroz', 'libra', 90.00, 0.55)
INSERT INTO PRODUCTOS VALUES(101, 'Azucar', 'libra', 38.00, 0.45)
INSERT INTO PRODUCTOS VALUES(102, 'Tomate', 'libra', 74.00, 0.5)
INSERT INTO PRODUCTOS VALUES(103, 'Carne', 'libra', 16.00, 2.3)
INSERT INTO PRODUCTOS VALUES(104, 'Atun', 'lata de 20 gr', 17.00, 1.5)
INSERT INTO PRODUCTOS VALUES(105, 'Aceite', 'litro', 94.00, 2.5)
INSERT INTO PRODUCTOS VALUES(106, 'Manzana', '10 unidades', 87.00, 1.2)
INSERT INTO PRODUCTOS VALUES(107, 'Pera', 'kg', 50.00, 2)
INSERT INTO PRODUCTOS VALUES(108, 'Man�', 'libra', 20.00, 1.5)

---Ingreso de Empleados----

select*from Empleados
insert into Empleados values('Luis',	'Rocha','01-01-2017',498)
insert into Empleados values('Marcelo',	'Armas','01-3-2016',500)
insert into Empleados values('Marta',	'Paez','12-02-2017',750)
---Ingreso Factura
select *from FACTURAS
INSERT INTO FACTURAS VALUES(1, 1, 7.25, '10/05/2017',100)
INSERT INTO FACTURAS VALUES(2, 1, 2.3, '01/04/2014',102)
INSERT INTO FACTURAS VALUES(7, 1, 4, '01/04/2014',100)
INSERT INTO FACTURAS VALUES(9, 2, 1.8, '01/04/2014',102)
INSERT INTO FACTURAS VALUES(11, 3, 8.6, '02/04/2014',100)
INSERT INTO FACTURAS VALUES(12, 4, 9.4, '30/04/2014',102)
INSERT INTO FACTURAS VALUES(15, 1, 10, '01/01/2015',102)
INSERT INTO FACTURAS VALUES(16, 3, 0, '30/04/2014',104)
INSERT INTO FACTURAS VALUES(20, 2, 15, '03/04/2014',104)
INSERT INTO FACTURAS VALUES(21, 8, 0, '13/04/2015',102)
INSERT INTO FACTURAS VALUES(22, 6, 0, '17/05/2014',104)
INSERT INTO FACTURAS VALUES(23, 1, 0, '01/01/2015',102)
INSERT INTO FACTURAS VALUES(28, 6, 0, '24/05/2014',100)
INSERT INTO FACTURAS VALUES(30, 7, 0, '14/04/2015',104)
INSERT INTO FACTURAS VALUES(32, 9, 0, '28/05/2015',104)


---Ingreso Detalle-----------
select*from DETALLES
INSERT INTO DETALLES VALUES(1, 100, 5.00, 0.55)
INSERT INTO DETALLES VALUES(1, 101, 10.00, 0.45)
INSERT INTO DETALLES VALUES(2, 100, 1.00, 0.5)
INSERT INTO DETALLES VALUES(2, 102, 2.00, 0.4)
INSERT INTO DETALLES VALUES(2, 106, 1.00, 1)
INSERT INTO DETALLES VALUES(7, 103, 2.00, 2)
INSERT INTO DETALLES VALUES(9, 101, 2.00, 0.4)
INSERT INTO DETALLES VALUES(9, 104, 1.00, 1)
INSERT INTO DETALLES VALUES(11, 102, 4.00, 0.4)
INSERT INTO DETALLES VALUES(11, 105, 2.00, 2.5)
INSERT INTO DETALLES VALUES(11, 106, 2.00, 1)
INSERT INTO DETALLES VALUES(12, 100, 4.00, 0.5)
INSERT INTO DETALLES VALUES(12, 103, 2.00, 2.2)
INSERT INTO DETALLES VALUES(12, 104, 2.00, 1.)
INSERT INTO DETALLES VALUES(15, 105, 4.00, 2.5)
INSERT INTO DETALLES VALUES(20, 106, 10.00, 1.5)


-------------------------------------CONSULTAS-----------------------

---2.	Desplegar el nombre y apellido de los clientes que no tienen facturas.
select*from Clientes
select*from FACTURAS
select distinct c.nombreCli, c.apellidoCli from CLIENTES c left outer join FACTURAS f on 
c.codigoCLi=f.codigoCLi where f.codigoCLi is null

---3.	Desplegar el nombre del producto menos vendido.
select*from Productos
select*from DETALLES

select d.codigoProd, p.nombreProd,count(*) AS 'Nro de Ventas' into #Temporal from DETALLES d , Productos p 
where d.codigoProd=p.codigoProd group by d.codigoProd,p.nombreProd
select nombreProd,[Nro de Ventas] from #Temporal where [Nro de Ventas]=(select min([Nro de Ventas]) from #Temporal)



---4.	Desplegar los datos de las facturas que fueron emitidas los fines de semana.
select*from FACTURAS
select distinct f.* from  FACTURAS f where (datename(dw,f.fechaFact)='S�bado') or (datename(dw,f.fechaFact)='Domingo')

---5.	Desplegar el nombre de cada producto junto al n�mero de facturas en las que se ha vendido.
select*from Productos
select*from DETALLES

select nombreProd, count(*) from PRODUCTOS p inner join DETALLES d
on p.codigoProd=d.codigoProd group by nombreProd

---6.	Nombre y apellido de clientes que viven en Ibarra y tienen m�s de una factura.
select*from FACTURAS
select*from CLIENTES
select nombreCli, apellidoCli, count(*) as 'Nro Factura' 
from CLIENTES c inner join FACTURAS f on c.codigoCLi=f.codigoCLi where c.CiudadCli='Ibarra'  group by nombreCli, apellidoCli
having count(*)>1
