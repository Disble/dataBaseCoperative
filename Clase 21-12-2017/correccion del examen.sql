DROP DATABASE ACAD
CREATE DATABASE ACAD
GO

USE ACAD

GO



CREATE TABLE ESTUDIANTES (
codEst CHAR(3) PRIMARY KEY,
nombreEst VARCHAR(100),
apellidoEst VARCHAR(100),
fechaNac DATE
)

CREATE TABLE MATERIAS (
codMat CHAR(3) PRIMARY KEY,
horaSemana INT,
nombreMateria VARCHAR(100)
)

CREATE TABLE ESTUDIANTE_MATERIA (
codMat CHAR(3),
codEst CHAR(3),
nota1 INT,
nota2 INT,
nota3 INT,
PRIMARY KEY(codMat,codEst),
FOREIGN KEY(codMat) REFERENCES MATERIAS (codMat),
FOREIGN KEY(codEst) REFERENCES ESTUDIANTES (codEst)
)

INSERT INTO ESTUDIANTES VALUES ('E01','	Ana','Puente','12-01-97')
INSERT INTO ESTUDIANTES VALUES ('E02','	Carlos','Fuertes','22-11-96')
INSERT INTO ESTUDIANTES VALUES ('E03','	Pedro','Andrade','27-07-97')

INSERT INTO MATERIAS VALUES ('M01',4,'Matemáticas')
INSERT INTO MATERIAS VALUES ('M02',5,'Física')
INSERT INTO MATERIAS VALUES ('M03',4,'Inglés')
INSERT INTO MATERIAS VALUES ('M04',6,'Química')

--Elimina los registros de una tabla
TRUNCATE TABLE ESTUDIANTE_MATERIA


INSERT INTO ESTUDIANTE_MATERIA VALUES ('M01','E01',10,10,0)
INSERT INTO ESTUDIANTE_MATERIA VALUES ('M01','E02',10,3,7)
INSERT INTO ESTUDIANTE_MATERIA VALUES ('M02','E01',6,6,6)
INSERT INTO ESTUDIANTE_MATERIA VALUES ('M02','E03',5,4,0)
INSERT INTO ESTUDIANTE_MATERIA VALUES ('M02','E02',8,8,0)
INSERT INTO ESTUDIANTE_MATERIA VALUES ('M03','E01',5,10,0)
INSERT INTO ESTUDIANTE_MATERIA VALUES ('M03','E03',7,6,9)
INSERT INTO ESTUDIANTE_MATERIA VALUES ('M04','E01',5,9,0)
INSERT INTO ESTUDIANTE_MATERIA VALUES ('M04','E02',10,10,0)
INSERT INTO ESTUDIANTE_MATERIA VALUES ('M04','E03',4,4,0)

--Desplegar  los nombres y apelldiso  de los estudiantes que tienen menos de 7 
--y sus aopellidos  inician con vocal 
 SELECT FROM ESTUDIANTES e, ESTUDIANTE_MATERIA em WHERE e.codEst = em.codEst
 AND LEFT (apellidoEst,1) = 'A' OR LEFT(apellidoEst,1)= 'E' OR LEFT (apellidoEst,1)='I' OR LEFT(apellidoEst,1) = 'O'
 OR LEFT (apellidoEst,1) = 'U' 
 --Consultar la frase : "El estudiante [nombre] [apellido] tomó la materia [nombreMateria] y sus notas
 -- fueron [nota1],[nota2],[nota3] y [supletorio]"
 SELECT 'El estudiante '+ nombreEst + space (2) + apellidoEst + ' tomó la materia ' + nombreMateria + 'y sus notas 
fueron nota 1 = ' + str(nota1,2)+' , nota 2 = '+ str(nota2,2)+ ' , supletorio = ' + str(nota3,2) FROM ESTUDIANTES e INNER JOIN 
 ESTUDIANTE_MATERIA em ON e.codEst = em.codEst INNER JOIN MATERIAS m ON em.codMat = m.codMat
 
 --Comentario agregado desde Gitlab :P