-- GESTI�N DE TRIGGERS
--Los ejemplos de este documento se realizan en la Base de datos de Biblioteca

--TRIGGER QUE EVITA QUE SE BORRE MAS DE UN CLIENTE A LA VEZ
create trigger tr_borrarClientes
on Clientes
after delete
as
if(select COUNT(*) from deleted)>1
begin
RAISERROR('NO PUEDE BORRAR MAS DE UN CLIENTE A LA VEZ',16,1)
rollback transaction
end

PROBANDO EL TRIGGER
select * from CLIENTES

insert into CLIENTES values('C05','Pedro','Paez',null),('C06','Marta','Jara',null),('C07','Marta','Ayala',null)


delete from CLIENTES where nombreCli='Marta' and codCli='C07'

select * from CLIENTES
select * from FACTURAS
select * from PRODUCTOS
select * from DETALLES


--PARA DESHABILITAR UN TRIGGER
Alter table DETALLES
disable trigger tr_INSERT_DETALLES

insert into DETALLES values('001','P01',4,0.90)

--PARA HABILITAR UN TRIGGER DESHABILITADO
Alter table DETALLES
enable trigger tr_INSERT_DETALLES

--PARA BORRAR UN TRIGGER
drop trigger tr_UPDATE_DETALLES

--Que triggers afectan a una tabla
exec sp_depends FACTURAS
exec sp_depends PRODUCTOS

--PARA OBTENER LA DEFINICION DE UN TRIGGER
exec sp_helptext tr_INSERT_DETALLES

--PARA VER LOS TRIGGERS QUE ESTAN ACTUANDO SOBRE UNA TABLA
exec sp_helptrigger DETALLES

--TRIGGERS INSTEAD OF (EN VEZ DE)
--Estos triggers ejecutan un grupo de instrucciones automaticamente cuando se trata de insertar, actualizar 
--o borrar una fila de una tabla, pero estas instrucciones se ejecutan en vez de la inserci�n, actualizaci�n 
--o borrado.
--Crear un trigger que impida borrar filas de la tabla hist�rico
create trigger tr_borrarDetalles
on Detalles instead of delete
as
raiserror('NO SE PUEDE BORRAR FILAS DE ESTA TABLA',10,1)

drop trigger tr_borrarProductos

SELECT * FROM Detalles

delete from Detalles where nroFact='001' 

PROCEDIMIENTOS ALMACENADOS
--Son conjuntos de instrucciones en SQL que se ejecutan en conjunto como un solo programa. Se utilizan cuando repetidamente
-- se ejecutan las mismas intrucciones. Son muy �tiles en administraci�n de bases de datos

--REALIZAR LA SIGUIENTE CONSULTA:
--Desplegar los nombres y apellidos de los clientes junto con el c�digo, valor y fecha de sus facturas
select nombreCliente,apellidoCliente, codigoFactura, valorFactura, fechaFactura
from Clientes c inner join Facturas f on c.codigoCliente=f.codigoCliente order by 2,1

-- Si esta consulta le hacemos con mucha frecuencia, es mejor crear un procedimiento almacenado con esta instruccion:
create proc sp_FacturasPorCliente
as
select nombreCliente,apellidoCliente, codigoFactura, valorFactura, fechaFactura
from Clientes c inner join Facturas f on c.codigoCliente=f.codigoCliente order by 2,1

exec sp_FacturasPorCliente

--PROCEDIMIENTOS ALMACENADOS CON PAR�METROS
--EJEMPLO:
--Desplegar los datos de las facturas del Cliente Carlos Puente
Select nombreCliente,apellidoCliente, f.*
from Clientes c inner join Facturas f on c.codigoCliente=f.codigoCliente 
where nombreCliente='Carlos' and apellidoCliente='Puente'

--Con un procedimiento almacenado se puede utilizar el c�digo para recuperar los datos de facturas de cualquier cliente
create proc sp_FacturasPorClienteConParametros
@nom varchar(20), @ape varchar(20)
as
Select nombreCliente,apellidoCliente, f.*
from Clientes c inner join Facturas f on c.codigoCliente=f.codigoCliente 
where nombreCliente=@nom and apellidoCliente=@ape

exec sp_FacturasPorClienteConParametros 'Carlos','Puente'

exec sp_FacturasPorClienteConParametros 'Abdala','Bucaram'

--Crear un procedimiento almacenado que despliegue los nombres de los productos
--y cantidades vendidas en cada factura, ingresando como par�metro el c�digo de la factura
create proc sp_ProductosPorFactura @codFact int
as
select codigoFactura, nombreProducto, cantidadVendida from Productos p
inner join Detalles d on p.codigoProducto=d.codigoProducto
where codigoFactura=@codFact

select * from Facturas

exec sp_ProductosPorFactura 1

exec sp_ProductosPorFactura 2
