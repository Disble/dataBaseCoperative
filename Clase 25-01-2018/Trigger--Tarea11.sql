SELECT * FROM PROPIETARIOS
SELECT * FROM REPARACIONES
SELECT * FROM REPUESTO_REPARACION
SELECT * FROM REPUESTOS
SELECT * FROM TRABAJO_REPARACION
SELECT * FROM TRABAJOS_REALIZADOS
SELECT * FROM VEHICULOS

-- 1. Crear un trigger que al insertar una fila en la tabla Trabajo_Reparacion
-- modifique el valorTotal en la tabla Reparaciones

-- DROP TRIGGER tr_INSERT_TRABAJO_REPARACION

CREATE TRIGGER tr_INSERT_TRABAJO_REPARACION
ON TRABAJO_REPARACION AFTER INSERT
AS
DECLARE @codigoRep CHAR(3), @codigoTra CHAR(3), @cantidadTrabajo INT, @valorTrab MONEY
SELECT @codigoRep=inserted.codigoRep, @codigoTra=inserted.codigoTrab, @cantidadTrabajo=inserted.cantidadTrabajo
FROM inserted

SELECT @valorTrab=tr.valorTrab
FROM TRABAJOS_REALIZADOS tr
WHERE tr.codigoTrab=@codigoTra

UPDATE REPARACIONES SET	valorTotalR=valorTotalR+(@valorTrab*@cantidadTrabajo)
WHERE codigoRep=@codigoRep

-- Vamos a agregar 10 de T1 a 400 de  R17
INSERT INTO TRABAJO_REPARACION VALUES('R17', 'T1', 1)

-- Vamos a agregar 4 * 5 de T4 a 100 de R07
INSERT INTO TRABAJO_REPARACION VALUES('R07', 'T4', 4)


-- 2. Crear un trigger que al borrar un fila en la tabla Trabajo_Reparacion
-- modifique el valorTotal en la tabla Reparaciones

CREATE TRIGGER tr_DELETE_TRABAJO_REPARACION
ON TRABAJO_REPARACION AFTER DELETE
AS
DECLARE @codigoRep CHAR(3), @codigoTra CHAR(3), @cantidadTrabajo INT, @valorTrab MONEY
SELECT @codigoRep=deleted.codigoRep, @codigoTra=deleted.codigoTrab, @cantidadTrabajo=deleted.cantidadTrabajo
FROM deleted

SELECT @valorTrab=tr.valorTrab
FROM TRABAJOS_REALIZADOS tr
WHERE tr.codigoTrab=@codigoTra

UPDATE REPARACIONES SET	valorTotalR=valorTotalR-(@valorTrab*@cantidadTrabajo)
WHERE codigoRep=@codigoRep

-- Vamos a quitar 4 * 5 de T4 a 100 de R07
DELETE TRABAJO_REPARACION WHERE codigoRep='R07' AND codigoTrab='T4'

-- 3. Crear un trigger que al actualizar la cantidad de una fila en la tabla Trabajo_Reparacion
-- modifique el valorTotal en la tabla Reparaciones

--DROP TRIGGER tr_UPDATE_TRABAJO_REPARACION

CREATE TRIGGER tr_UPDATE_TRABAJO_REPARACION
ON TRABAJO_REPARACION AFTER UPDATE
AS
DECLARE @codigoRep CHAR(3), @codigoTra CHAR(3), @cantidadTrabajo INT, @valorTrab MONEY
SELECT @codigoRep=inserted.codigoRep, @codigoTra=inserted.codigoTrab, @cantidadTrabajo=inserted.cantidadTrabajo
FROM inserted

DECLARE @dcodigoRep CHAR(3), @dcodigoTra CHAR(3), @dcantidadTrabajo INT, @dvalorTrab MONEY
SELECT @dcodigoRep=deleted.codigoRep, @dcodigoTra=deleted.codigoTrab, @dcantidadTrabajo=deleted.cantidadTrabajo
FROM deleted

SELECT @dvalorTrab=tr.valorTrab
FROM TRABAJOS_REALIZADOS tr
WHERE tr.codigoTrab=@dcodigoTra

UPDATE REPARACIONES SET	valorTotalR=valorTotalR-(@dvalorTrab*@dcantidadTrabajo)
WHERE codigoRep=@dcodigoRep

SELECT @valorTrab=tr.valorTrab
FROM TRABAJOS_REALIZADOS tr
WHERE tr.codigoTrab=@codigoTra

UPDATE REPARACIONES SET	valorTotalR=valorTotalR+(@valorTrab*@cantidadTrabajo)
WHERE codigoRep=@codigoRep

-- Vamos a agregar 4 * 5 de T4 a 100 de R07
INSERT INTO TRABAJO_REPARACION VALUES('R07', 'T4', 4)
-- Vamos a actualizar 4 * 5 de T4 y 120 de R07 a 100 de R07 a 2 * 10 de T1 y 410 de R17
UPDATE TRABAJO_REPARACION 
SET codigoRep='R17', codigoTrab='T1', cantidadTrabajo=2
WHERE codigoRep='R07' AND codigoTrab='T4' AND cantidadTrabajo=4


-- 4. Crear un trigger que al borrar un fila en la tabla Repuesto_Reparacion
-- modifique el valorTotal en la tabla Reparaciones

CREATE TRIGGER tr_DELETE_REPUESTO_REPARACION
ON REPUESTO_REPARACION AFTER DELETE
AS
DECLARE @codigoRep CHAR(3), @codigoRto CHAR(3), @cantidadRep INT, @valorRto MONEY
SELECT @codigoRep=deleted.codigoRep, @codigoRto=deleted.codigoRto, @cantidadRep=deleted.cantidadRep
FROM deleted

SELECT @valorRto=r.valorRto
FROM REPUESTOS r
WHERE r.codigoRto=@codigoRto

UPDATE REPARACIONES SET	valorTotalR=valorTotalR-(@valorRto*@cantidadRep)
WHERE codigoRep=@codigoRep

-- Vamos a quitar 200 * 1 de R7 (repuesto) a 1008 de R18 (reparacion)
DELETE REPUESTO_REPARACION
WHERE codigoRep='R18' AND codigoRto='R7'

-- 5. Crear un trigger que al actualizar la cantidad de una fila en la tabla Repuesto_Reparacion
-- modifique el valorTotal en la tabla Reparaciones

CREATE TRIGGER tr_UPDATE_REPUESTO_REPARACION
ON REPUESTO_REPARACION AFTER UPDATE
AS
DECLARE @codigoRep CHAR(3), @codigoRto CHAR(3), @cantidadRep INT, @valorRto MONEY
SELECT @codigoRep=inserted.codigoRep, @codigoRto=inserted.codigoRto, @cantidadRep=inserted.cantidadRep
FROM inserted

DECLARE @dcodigoRep CHAR(3), @dcodigoRto CHAR(3), @dcantidadRep INT, @dvalorRto MONEY
SELECT @dcodigoRep=deleted.codigoRep, @dcodigoRto=deleted.codigoRto, @dcantidadRep=deleted.cantidadRep
FROM deleted

SELECT @dvalorRto=r.valorRto
FROM REPUESTOS r
WHERE r.codigoRto=@dcodigoRto

UPDATE REPARACIONES SET	valorTotalR=valorTotalR-(@dvalorRto*@dcantidadRep)
WHERE codigoRep=@dcodigoRep

SELECT @valorRto=r.valorRto
FROM REPUESTOS r
WHERE r.codigoRto=@codigoRto

UPDATE REPARACIONES SET	valorTotalR=valorTotalR+(@valorRto*@cantidadRep)
WHERE codigoRep=@codigoRep

-- Vamos a actualizar 300 * 2 de R6 (repuesto) y 430 de R17 (reparacion)
-- a 200 * 1 de R7 (repuesto) a 808 de R18 (reparacion)
UPDATE REPUESTO_REPARACION
SET codigoRep='R18', codigoRto='R7', cantidadRep=1
WHERE codigoRep='R17' AND codigoRto='R6' AND cantidadRep=2