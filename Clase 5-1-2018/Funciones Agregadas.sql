--FUNCIONES AGREGADAS
--MAX(), MIN(), AVG(), COUNT(), SUM()

--Desplegar el valor m�ximo pagado por una Reparaci�n.
SELECT MAX(valorTotalR) FROM REPARACIONES

--Desplegar el valor m�nimo promedio pagado por una Reparaci�n
SELECT MIN(valorTotalR) FROM REPARACIONES

--Desplegar el valor promedio pagado por una Reparaci�n
SELECT AVG(valorTotalR) FROM REPARACIONES

--Desplegar el n�mero de filas de la tabla Reparaciones
SELECT COUNT(*) FROM REPARACIONES

--Desplegar la suma de valores pagados por Repraraciones
SELECT SUM(valorTotal) FROM REPARACIONES

--SUBCONSULTAS
--Desplegar los datos del veh�culo que mas pag� por una reparaci�n
SELECT * FROM VEHICULOS v, REPARACIONES r
WHERE v.placaV=r.placaV AND valorTotalR=(SELECT MAX(valorTotalR) FROM REPARACIONES)

--Desplegar la placa de cada veh�culo, el c�digo de reparaci�n y el valor pagado por cada reparaci�n
SELECT v.placaV, r.codigoRep, r.valorTotalR
FROM VEHICULOS v, REPARACIONES r
WHERE v.placaV=r.placaV
	--Versi�n Ing
SELECT placaV, codigoRep, valorTotalR FROM REPARACIONES

--Desplegar el n�mero de veces que se ha reparado cada veh�culo
SELECT placaV, COUNT(*) AS 'Nro Reparaciones' 
FROM REPARACIONES GROUP BY placaV

--Desplegar el total pagado por cada veh�culo en reparaciones
SELECT placaV, SUM(valorTotalR) AS 'Suma de valorTotalR' 
FROM REPARACIONES GROUP BY placaV

--Desplegar el nombre y apellido de cada propietario junto con el n�mero de veh�culos que posee
SELECT p.nombreProp, p.apellidoProp, COUNT(*) AS 'Nro Veh�culos'
FROM PROPIETARIOS p, VEHICULOS v
WHERE p.ciProp=v.ciProp
GROUP BY p.nombreProp, p.apellidoProp
	--Version Ing
select nombreProp, apellidoProp, count(*) as 'Nro Veh�culos'
from PROPIETARIOS p inner join VEHICULOS v on p.ciProp=v.ciProp
group by nombreProp, apellidoProp

--Desplegar el nombre y apellido del propietario del veh�culo que menos pag� por una reparaci�n
SELECT p.nombreProp, p.apellidoProp
FROM PROPIETARIOS p, VEHICULOS v, REPARACIONES r
WHERE p.ciProp=v.ciProp AND v.placaV=r.placaV AND r.valorTotalR=(SELECT MIN(valorTotalR) FROM REPARACIONES)

--Desplegar los datos del veh�culo que mas repuestos utiliz� en una reparaci�n
SELECT v.*--, rr.cantidadRep
FROM PROPIETARIOS p, VEHICULOS v, REPARACIONES r, REPUESTO_REPARACION rr
WHERE p.ciProp=v.ciProp AND v.placaV=r.placaV AND r.codigoRep=rr.codigoRep 
	AND rr.cantidadRep=(SELECT MAX(cantidadRep) FROM REPUESTO_REPARACION)

--Desplegar los datos de los veh�culos cuya reparaci�n cost� mas que el promedio
SELECT v.* --, r.valorTotalR
FROM PROPIETARIOS p, VEHICULOS v, REPARACIONES r
WHERE p.ciProp=v.ciProp AND v.placaV=r.placaV AND r.valorTotalR>(SELECT AVG(valorTotalR) FROM REPARACIONES)
	--Version Ing
select v.*, codigoRep, valorTotalR from VEHICULOS v inner join REPARACIONES r on v.placaV=r.placaV
where valorTotalR > (select AVG(valorTotalR) from REPARACIONES)

--Desplegar el nombre y apellido de propietarios que tienen mas de dos vehiculos
SELECT *
FROM PROPIETARIOS p, VEHICULOS v
WHERE p.ciProp=v.ciProp AND r.valorTotalR=(SELECT COUNT(ciProp) FROM VEHICULOS) --Incompleto

	--Version Ing
select nombreProp, apellidoProp, count(*) from PROPIETARIOS p inner join VEHICULOS v
on p.ciProp=v.ciProp group by nombreProp, apellidoProp having count(*)>2
--"having es un especie de where cuando utilizamos funciones agregadas" 2018

--Desplegar las placas de los veh�culos marca Nissan que han tenido mas de una reparacion
	--Version Ing
select v.placaV, marcaV, count(*) from VEHICULOS v, REPARACIONES r where v.placaV=r.placaV
and marcaV='Nissan' group by v.placaV, marcaV having count(*)>1

--Desplegar el total de veces que se ha realizado cada trabajo en diciembre del 2017
	--Version Ing
select descripcionTrab, sum(cantidadTrabajo) from TRABAJOS_REALIZADOS tr, TRABAJO_REPARACION tp,
REPARACIONES r
where tr.codigoTrab=tp.codigoTrab and tp.codigoRep=r.codigoRep and MONTH(fechaEntrada)=12
and YEAR(fechaEntrada)=2017 group by descripcionTrab

--Desplegar el nombre y apellido del propietario que tiene mas veh�culos
SELECT nombreProp, apellidoProp, MAX(placaV)
FROM PROPIETARIOS p, VEHICULOS v
WHERE p.ciProp=v.ciProp
GROUP BY nombreProp, apellidoProp HAVING p.ciProp=((SELECT nombreProp, apellidoProp, COUNT(*)
FROM PROPIETARIOS p, VEHICULOS v
WHERE p.ciProp=v.ciProp
GROUP BY nombreProp, apellidoProp)) --No funciona

	--Version Ing
select nombreProp, apellidoProp, count(*) as 'Nro Veh�culos' into #Temporal 
from PROPIETARIOS p inner join VEHICULOS v on p.ciProp=v.ciProp 
group by nombreProp, apellidoProp

select nombreProp, apellidoProp from #Temporal where [Nro Veh�culos]=
(select max([Nro Veh�culos]) from #Temporal)

drop table #Temporal
