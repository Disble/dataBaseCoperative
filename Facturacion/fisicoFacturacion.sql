CREATE DATABASE Facturacion

USE Facturacion

CREATE TABLE CLIENTES (
	codCliente INT PRIMARY KEY,
	nombreCliente VARCHAR(20),
	apellidoCliente VARCHAR(20),
	ciudadCliente VARCHAR(15),
)

CREATE TABLE PRODUCTOS (
	codProducto INT PRIMARY KEY,
	nombreProducto VARCHAR(20),
	unidadesMedidaProducto VARCHAR(20),
	cantidadStockProducto INT,
	precioUnitarioActualProducto FLOAT
)

CREATE TABLE FACTURAS (
	codFactura INT PRIMARY KEY,
	codCliente INT REFERENCES CLIENTES(codCliente),
	valorTotalFactura FLOAT,
	fechaFactura DATE
)

CREATE TABLE DETALLES (
	codDetalle INT PRIMARY KEY,
	codFactura INT REFERENCES FACTURAS(codFactura),
	codProducto INT REFERENCES PRODUCTOS(codProducto),
	valorUnitarioProductoDetalle FLOAT,
	cantidadDetalle INT
)

-- Insertar Cliente
SELECT * FROM CLIENTES

INSERT INTO CLIENTES VALUES(1, 'Alma', 'Hilda', 'Ibarra')
INSERT INTO CLIENTES VALUES(2, 'Bryan', 'Miguel', 'Otalavo')
INSERT INTO CLIENTES VALUES(3, 'Carlos', 'Xilonen', 'Quito')
INSERT INTO CLIENTES VALUES(4, 'Daniel', 'Zuria', 'Riobamaba')
INSERT INTO CLIENTES VALUES(5, 'Erick', 'Omar', 'Santo Domingo')

-- Insertar Productos
SELECT * FROM PRODUCTOS

INSERT INTO PRODUCTOS VALUES(1, 'L�piz', 'centimetros', 100, 0.25)
INSERT INTO PRODUCTOS VALUES(2, 'Esferogr�fico', 'centimetros', 150, 0.30)
INSERT INTO PRODUCTOS VALUES(3, 'Cuaderno', 'centimetros', 500, 1.25)
INSERT INTO PRODUCTOS VALUES(4, 'Calculadora', 'centimetros', 50, 8.00)
INSERT INTO PRODUCTOS VALUES(5, 'Corrector', 'centimetros', 200, 0.50)

-- Insertar Facturas
SELECT * FROM FACTURAS

INSERT INTO FACTURAS VALUES(1, 1, 0.55, '1-11-2017')
INSERT INTO FACTURAS VALUES(2, 2, 9.25, '2-11-2017')
INSERT INTO FACTURAS VALUES(3, 3, 2.00, '3-11-2017')
INSERT INTO FACTURAS VALUES(4, 4, 8.30, '3-11-2017')
INSERT INTO FACTURAS VALUES(5, 5, 1.50, '4-11-2017')
INSERT INTO FACTURAS VALUES(6, 2, 8.25, '5-11-2017')
INSERT INTO FACTURAS VALUES(7, 3, 9.55, '5-11-2017')
INSERT INTO FACTURAS VALUES(8, 5, 2.05, '5-11-2017')

-- Insertar Detalles
SELECT * FROM DETALLES

INSERT INTO DETALLES VALUES(1, 1, 1, 0.25, 1)
INSERT INTO DETALLES VALUES(2, 1, 2, 0.30, 2)
INSERT INTO DETALLES VALUES(3, 2, 3, 1.25, 3)
INSERT INTO DETALLES VALUES(4, 2, 4, 8.00, 1)
INSERT INTO DETALLES VALUES(5, 3, 5, 0.50, 5)
INSERT INTO DETALLES VALUES(6, 3, 1, 0.25, 3)
INSERT INTO DETALLES VALUES(7, 3, 3, 1.25, 4)
INSERT INTO DETALLES VALUES(8, 4, 4, 8.00, 1)
INSERT INTO DETALLES VALUES(9, 4, 2, 0.30, 3)
INSERT INTO DETALLES VALUES(10, 5, 1, 0.25, 4)
INSERT INTO DETALLES VALUES(11, 5, 3, 1.25, 4)
INSERT INTO DETALLES VALUES(12, 6, 5, 0.50, 3)
INSERT INTO DETALLES VALUES(13, 6, 4, 8.00, 1)
INSERT INTO DETALLES VALUES(14, 6, 1, 0.25, 2)
INSERT INTO DETALLES VALUES(15, 7,  2, 0.30, 1)
INSERT INTO DETALLES VALUES(16, 7, 3, 1.25, 3)
INSERT INTO DETALLES VALUES(17, 7, 4, 8.00, 1)
INSERT INTO DETALLES VALUES(18, 8, 5, 0.50, 4)
INSERT INTO DETALLES VALUES(19, 8, 2, 0.30, 5)
INSERT INTO DETALLES VALUES(20, 8, 3, 1.25, 1)
