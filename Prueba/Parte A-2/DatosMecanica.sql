create database mecanica
use mecanica
go

CREATE TABLE PROPIETARIOS (
	ciProp CHAR(10) PRIMARY KEY,
	nombreProp VARCHAR(20),
	apellidoProp VARCHAR(20),
	telefProp CHAR(10)
)

CREATE TABLE VEHICULOS (
	placaV CHAR(10) PRIMARY KEY,
	tipoV VARCHAR(10),
	colorV VARCHAR(10),
	a�oFabricV INT,
	ciProp CHAR(10),
	FOREIGN KEY(ciProp) REFERENCES PROPIETARIOS (ciProp)
)

CREATE TABLE REPARACIONES (
	codigoRep CHAR(3) PRIMARY KEY,
	fechaEntrada DATE,
	fechaSalida DATE,
	placaV CHAR(10),
	FOREIGN KEY(placaV) REFERENCES VEHICULOS (placaV)
)

CREATE TABLE TRABAJOS_REALIZADOS (
	codigoTrab CHAR(3) PRIMARY KEY,
	descripcionTrab VARCHAR(50),
	valorTrab MONEY
)

CREATE TABLE REPUESTOS (
	codigoRto CHAR(3) PRIMARY KEY,
	descripcionRto VARCHAR(50),
	valorRto MONEY
)

CREATE TABLE TRABAJO_REPARACION (
	codigoRep CHAR(3),
	codigoTrab CHAR(3),
	cantidadTrabajo INT,
	FOREIGN KEY(codigoRep) REFERENCES REPARACIONES (codigoRep),
	FOREIGN KEY(codigoTrab) REFERENCES TRABAJOS_REALIZADOS (codigoTrab)
)

CREATE TABLE REPUESTO_REPARACION (
	codigoRep CHAR(3),
	codigoRto CHAR(3),
	cantidadRep INT,
	PRIMARY KEY(codigoRep, codigoRto),
	FOREIGN KEY(codigoRep) REFERENCES REPARACIONES (codigoRep),
	FOREIGN KEY(codigoRto) REFERENCES REPUESTOS (codigoRto)
)

SELECT * FROM PROPIETARIOS

INSERT INTO PROPIETARIOS VALUES('1001396780', 'Ana', 'Corrales', '0983456780')
INSERT INTO PROPIETARIOS VALUES('1701396551', 'Juan', 'L�pez', '0998056112')

SELECT * FROM VEHICULOS

INSERT INTO VEHICULOS VALUES('IDC-3425', 'Cami�n', 'rojo', 1997, '1001396780')
INSERT INTO VEHICULOS VALUES('PPF-4511', 'Automovil', 'azul', 2017, '1001396780')
INSERT INTO VEHICULOS VALUES('ICD-0978', 'Automovil', 'blanco', 2010, '1001396780')

SELECT * FROM TRABAJOS_REALIZADOS

INSERT INTO TRABAJOS_REALIZADOS VALUES('T1', 'Cambio de aceite de motor', 10.0)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T2', 'Alineacion de llantas', 11.0)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T3', 'Limpieza de inyectores', 8.0)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T4', 'Cambio de filtros', 5.0)

SELECT * FROM REPUESTOS

INSERT INTO REPUESTOS VALUES('R1', 'Filtro de aceite', 8.0)
INSERT INTO REPUESTOS VALUES('R2', 'Filtro de aire', 15.0)
INSERT INTO REPUESTOS VALUES('R3', 'Llanta', 45.0)
INSERT INTO REPUESTOS VALUES('R4', 'Foco delantero', 5.0)

SELECT * FROM REPARACIONES

INSERT INTO REPARACIONES VALUES('R01', '01-12-2017', '01-13-2017', 'IDC-3425')
INSERT INTO REPARACIONES VALUES('R02', '01-12-17', '01-12-17', 'PPF-4511')
INSERT INTO REPARACIONES VALUES('R03', '01-15-17', '01-18-17', 'ICD-0978')
INSERT INTO REPARACIONES VALUES('R04', '01-17-17', '01-18-17', 'IDC-3425')

SELECT * FROM TRABAJO_REPARACION

INSERT INTO TRABAJO_REPARACION VALUES('R01', 'T1', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R01', 'T2', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R02', 'T1', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R02', 'T3', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R03', 'T1', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R04', 'T3', 1)

SELECT * FROM REPUESTO_REPARACION

INSERT INTO REPUESTO_REPARACION VALUES('R01', 'R1', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R01', 'R2', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R02', 'R1', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R03', 'R3', 4)
INSERT INTO REPUESTO_REPARACION VALUES('R04', 'R4', 1)

SELECT * FROM REPARACIONES

-- Para insertar un nuevo atributo en la tabla
ALTER TABLE REPARACIONES ADD valorTotalR MONEY
--ALTER TABLE REPARACIONES DROP COLUMN valorTotal

-- Para modificar el valorTotal
UPDATE REPARACIONES SET valorTotalR=45
WHERE codigoRep='R01'

UPDATE REPARACIONES SET valorTotalR=8+10+16
WHERE codigoRep='R02'

UPDATE REPARACIONES SET valorTotalR=4*45+10
WHERE codigoRep='R03'

UPDATE REPARACIONES SET valorTotalR=5+8
WHERE codigoRep='R04'

-- CONSULTAS B�SICAS CON SQL Y ALGEBRA RELACIONAL
-- Desplegar los datos de los veh�culos de color rojo
SELECT * FROM VEHICULOS WHERE colorV='rojo'

-- Desplegarlos datos de los veh�culos de color blanco cuyo a�o de fabricaci�n sea menor que el 2005
SELECT * FROM VEHICULOS WHERE colorV='blanco' AND a�oFabricV<2005

-- Desplegarlos datos de los veh�culos de color blanco o vehiculos cuyo a�o de fabricaci�n sea menor que el 2005
SELECT * FROM VEHICULOS WHERE colorV='blanco' OR a�oFabricV<2005

-- Desplegar la placa y el tipo  de los vehiculos.
SELECT placaV, tipoV FROM VEHICULOS

-- Por si...
--ALTER TABLE VEHICULOS
--   ALTER COLUMN a�oFabricV INT

-- Desplegar la placa de los vehiculos con a�o de fabricacion amyor que 2000
SELECT placaV FROM VEHICULOS WHERE a�oFabricV>2000

-- Desplegar el codigo y descripcion de los repuestos que cuestan menos de 10 d�lares
SELECT  codigoRto, descripcionRto FROM REPUESTOS WHERE valorRto<10

-- Desplegar las placas de los vehiculos que ingresaron al taller el 2 de enero de 2017

-- PRODUCTO CARTESIANO

--SELECT * FROM PROPIETARIOS CROSS JOIN VEHICULOS
--WHERE PROPIETARIOS.ciProp=VEHICULOS.ciProp

--SELECT * FROM PROPIETARIOS, VEHICULOS --Whats the difference?

SELECT * FROM PROPIETARIOS p INNER JOIN VEHICULOS v
ON p.ciProp=v.ciProp

-- Desplegar el nombre y apellido del propietario de automovil azul
SELECT nombreProp, apellidoProp FROM PROPIETARIOS p INNER JOIN VEHICULOS v
ON p.ciProp=v.ciProp WHERE tipoV='automovil' AND colorV='azul'

-- SELECT nombreProp, apellidoProp FROM PROPIETARIOS p, VEHICULOS v
--WHERE p.ciProp=v.ciProp AND tipoV='automovil' AND colorV='azul' -- Whats the difference?

--PRODUCTO NATURAL
--Desplegar las placas de los veh�culos que pusieron llantas nuevas en el taller
SELECT placaV FROM REPUESTOS r INNER JOIN REPUESTO_REPARACION rr ON r.codigoRto=rr.codigoRto
INNER JOIN REPARACIONES rp ON rr.codigoRep=rp.codigoRep WHERE descripcionRto='llanta'

--Desplegar los nombres y apellidos de los propietarios de vehiculos que limpiaron los inyectores
--en el taller
SELECT DISTINCT(p.nombreProp), p.apellidoProp FROM PROPIETARIOS p INNER JOIN VEHICULOS v
ON p.ciProp=v.ciProp INNER JOIN REPARACIONES r ON v.placaV=r.placaV INNER JOIN TRABAJO_REPARACION tr
ON r.codigoRep=tr.codigoRep INNER JOIN TRABAJOS_REALIZADOS tre ON tr.codigoTrab=tre.codigoTrab
WHERE tre.descripcionTrab='Limpieza de inyectores' -- Este es mio :)

SELECT DISTINCT(nombreProp), apellidoProp FROM PROPIETARIOS p, VEHICULOS v,
REPARACIONES r, TRABAJO_REPARACION tr, TRABAJOS_REALIZADOS tt
WHERE p.ciProp=v.ciProp AND v.placaV=r.placaV AND r.codigoRep=tr.codigoRep
AND tr.codigoTrab=tt.codigoTrab AND descripcionTrab='Limpieza de inyectores'

SELECT * FROM PROPIETARIOS WHERE 1=1

select * from PROPIETARIOS 
alter table PROPIETARIOS add ciudadPropietario varchar(100) 
update PROPIETARIOS set ciudadPropietario='Ibarra' where nombreProp='Ana' 
update PROPIETARIOS set ciudadPropietario='Otavalo' where nombreProp='Juan'

insert into PROPIETARIOS values('1004859664','Fernando','Castro', '0993410250','Quito') 
insert into PROPIETARIOS values('1009865314','Natalia','Jimenez','0987657788', 'Ibarra') 
insert into PROPIETARIOS values('1003579754','Lizeth','Maldonado','098675912', 'Otavalo') 
insert into PROPIETARIOS values('1003534543','Kevin','Castro','0984676720', 'Ibarra') 
insert into PROPIETARIOS values('1003984839','Nicolas','Tatamuez','0986768204', 'Quito')

select * from VEHICULOS 
insert into VEHICULOS values('ICE-3286','Camion','blanco',2010,'1004859664') 
insert into VEHICULOS values('ABC-4671','Camioneta','azul',2011,'1004859664') 
insert into VEHICULOS values('CBE-5423','automovil','verde',2000,'1004859664') 
insert into VEHICULOS values('DAE-3191','Camioneta','rojo',199,'1009865314') 
insert into VEHICULOS values('IAE-1122','Camioneta','rojo',2016,'1009865314') 
insert into VEHICULOS values('INE-1097','automovil','azul',2014,'1003579754') 
insert into VEHICULOS values('JNV-2568','bus','plomo',2015,'1003579754') 
insert into VEHICULOS values('CNA-1100','Camioneta','negro',2016,'1003534543')
insert into VEHICULOS values('IPT-3290','bus','plomo',2009,'1003984839') 
insert into VEHICULOS values('ICD-3425','Camion','rojo',1997,'1001396780') 
insert into VEHICULOS values('ICD-0978','Automovil','blanco',2010,'1701396551')

select * from VEHICULOS 
alter table VEHICULOS add marcaV varchar(100)
update VEHICULOS set marcaV='Kia' where placaV='ABC-4671' 
update VEHICULOS set marcaV='Nissan' where placaV='CBE-5423' 
update VEHICULOS set marcaV='Nissan' where placaV='CNA-1100' 
update VEHICULOS set marcaV='Chevrolet' where placaV='DAE-3191' 
update VEHICULOS set marcaV='Toyota' where placaV='IAE-1122' 
update VEHICULOS set marcaV='Kia' where placaV='ICD-0978' 
update VEHICULOS set marcaV='Toyota' where placaV='ICD-3425' 
update VEHICULOS set marcaV='Nissan' where placaV='ICE-3286' 
update VEHICULOS set marcaV='Chevrolet' where placaV='INE-1097' 
update VEHICULOS set marcaV='Nissan' where placaV='IPT-3290' 
update VEHICULOS set marcaV='Kia' where placaV='JNV-2568' 
update VEHICULOS set marcaV='Toyota' where placaV='PPF-4511'

select * from REPUESTOS 
insert into REPUESTOS values ('R5','Motor',800) 
insert into REPUESTOS values ('R6','Tramsmisi�n',300) 
insert into REPUESTOS values ('R7','embrage', 200)

select * from TRABAJOS_REALIZADOS 
insert into TRABAJOS_REALIZADOS values('T5','Desmontaje de motor',100) 
insert into TRABAJOS_REALIZADOS values('T6','Instalaci�n de motor nuevo',300) 
insert into TRABAJOS_REALIZADOS values('T7','Remplazo de transmisi�n',200) 
insert into TRABAJOS_REALIZADOS values('T8','Cambio de embrague',100)

select * from VEHICULOS 
select * from REPARACIONES 
insert into REPARACIONES values('R01','2017-06-12','2017-06-13','ABC-4671',0) 
insert into REPARACIONES values('R02','2017-07-21','2017-07-22','ABC-4671',0) 
insert into REPARACIONES values('R03','2017-03-19','2017-03-25','CBE-5423',0) 
insert into REPARACIONES values('R04','2017-01-20','2017-01-29','CBE-5423',0) 
insert into REPARACIONES values('R05','2017-09-05','2017-09-07','CNA-1100',0) 
insert into REPARACIONES values('R06','2017-02-11','2017-02-12','CNA-1100',0) 
insert into REPARACIONES values('R07','2017-04-05','2017-04-06','DAE-3191',0) 
insert into REPARACIONES values('R08','2017-05-10','2017-05-15','DAE-3191',0) 
insert into REPARACIONES values('R09','2017-03-17','2017-03-18','DAE-3191',0) 
insert into REPARACIONES values('R10','2017-08-17','2017-08-19','IAE-1122',0) 
insert into REPARACIONES values('R11','2017-02-20','2017-02-21','ICD-0978',0) 
insert into REPARACIONES values('R12','2017-02-20','2017-02-28','ICD-3425',0) 
insert into REPARACIONES values('R13','2017-02-22','2017-02-24','ICE-3286',0) 
insert into REPARACIONES values('R14','2017-03-01','2017-03-04','INE-1097',0) 
insert into REPARACIONES values('R15','2017-03-10','2017-03-12','IPT-3290',0) 

select * from REPUESTO_REPARACION 
select * from REPUESTOS 
insert into REPUESTO_REPARACION values ('R01','R4',2)
insert into REPUESTO_REPARACION values ('R01','R7',1)
insert into REPUESTO_REPARACION values ('R02','R3',2) 
insert into REPUESTO_REPARACION values ('R03','R7',1)
insert into REPUESTO_REPARACION values ('R03','R1',2)
insert into REPUESTO_REPARACION values ('R03','R5',1)
insert into REPUESTO_REPARACION values ('R04','R6',1) 
insert into REPUESTO_REPARACION values ('R05','R1',1)
insert into REPUESTO_REPARACION values ('R06','R5',1) 
insert into REPUESTO_REPARACION values ('R07','R3',1) 
insert into REPUESTO_REPARACION values ('R08','R4',1)
insert into REPUESTO_REPARACION values ('R08','R2',1)
insert into REPUESTO_REPARACION values ('R09','R2',1) 
insert into REPUESTO_REPARACION values ('R10','R7',1) 
insert into REPUESTO_REPARACION values ('R11','R4',1) 
insert into REPUESTO_REPARACION values ('R12','R1',1) 
insert into REPUESTO_REPARACION values ('R13','R3',3)
insert into REPUESTO_REPARACION values ('R13','R7',2)
insert into REPUESTO_REPARACION values ('R13','R2',1)
insert into REPUESTO_REPARACION values ('R14','R2',1) 
insert into REPUESTO_REPARACION values ('R15','R7',1)
insert into REPUESTO_REPARACION values ('R15','R5',1)
insert into REPUESTO_REPARACION values ('R16','R2',1) 
select *from REPARACIONES 
select *from TRABAJOS_REALIZADOS
insert into TRABAJO_REPARACION values ('R01','T4',1)
insert into TRABAJO_REPARACION values ('R01','T8',1)
insert into TRABAJO_REPARACION values ('R02','T2',2) 
insert into TRABAJO_REPARACION values ('R03','T4',2)
insert into TRABAJO_REPARACION values ('R03','T1',2)
insert into TRABAJO_REPARACION values ('R03','T6',1)
insert into TRABAJO_REPARACION values ('R04','T1',3) 
insert into TRABAJO_REPARACION values ('R05','T2',1)
insert into TRABAJO_REPARACION values ('R06','T5',2) 
insert into TRABAJO_REPARACION values ('R07','T3',1) 
insert into TRABAJO_REPARACION values ('R08','T4',5)
insert into TRABAJO_REPARACION values ('R08','T2',1)
insert into TRABAJO_REPARACION values ('R09','T2',1) 
insert into TRABAJO_REPARACION values ('R10','T7',1) 
insert into TRABAJO_REPARACION values ('R11','T4',4) 
insert into TRABAJO_REPARACION values ('R12','T1',1) 
insert into TRABAJO_REPARACION values ('R13','T2',3)
insert into TRABAJO_REPARACION values ('R13','T1',1)
insert into TRABAJO_REPARACION values ('R13','T6',1)
insert into TRABAJO_REPARACION values ('R14','T3',1) 
insert into TRABAJO_REPARACION values ('R15','T8',1)
insert into TRABAJO_REPARACION values ('R15','T1',3)
insert into TRABAJO_REPARACION values ('R16','T6',2)
insert into TRABAJO_REPARACION values ('R12','T2',1) 
insert into TRABAJO_REPARACION values ('R13','T3',3)
insert into TRABAJO_REPARACION values ('R13','T7',3)
insert into TRABAJO_REPARACION values ('R13','T8',1)
insert into TRABAJO_REPARACION values ('R14','T2',2) 
insert into TRABAJO_REPARACION values ('R15','T6',1)
insert into TRABAJO_REPARACION values ('R15','T5',1)
insert into TRABAJO_REPARACION values ('R16','T7',1) 

 
select * from TRABAJO_REPARACION 
select * from TRABAJOS_REALIZADOS 
select * from REPUESTO_REPARACION 
select * from REPUESTOS
update REPARACIONES set valorTotalR=10+100+2*5 where codigoRep='R01' 
update REPARACIONES set valorTotalR=2*5+2*45 where codigoRep='R02' 
update REPARACIONES set valorTotalR=200+200 where codigoRep='R03'
update REPARACIONES set valorTotalR=100+8+300 where codigoRep='R04' 
update REPARACIONES set valorTotalR=300+800 where codigoRep='R05' 
update REPARACIONES set valorTotalR=2*8+45 where codigoRep='R6' 
update REPARACIONES set valorTotalR=11+5 where codigoRep='R07' 
update REPARACIONES set valorTotalR=200+15 where codigoRep='R08' 
update REPARACIONES set valorTotalR=300+200 where codigoRep='R09' 
update REPARACIONES set valorTotalR=10+5 where codigoRep='R10' 
update REPARACIONES set valorTotalR=8+8 where codigoRep='R11' 
update REPARACIONES set valorTotalR=100+3*45 where codigoRep='R12' 
update REPARACIONES set valorTotalR=10+11+15 where codigoRep='R13' 
update REPARACIONES set valorTotalR=100+200 where codigoRep='R14' 
update REPARACIONES set valorTotalR=8+15
where codigoRep='R15'


--Desplegar los trabajos realizados y los repuestos junto a la placa y fecha de ingreso 
--de los vehiculos que ingresaron al taller en Enero del 2017 tipo camioneta.

SELECT trr.descripcionTrab, re.descripcionRto, r.placaV, r.fechaEntrada, v.tipoV
FROM VEHICULOS v, REPARACIONES r, TRABAJO_REPARACION tr, REPUESTO_REPARACION rr, TRABAJOS_REALIZADOS trr, REPUESTOS re
WHERE v.placaV=r.placaV AND r.codigoRep=tr.codigoRep AND rr.codigoRep=tr.codigoRep 
	AND trr.codigoTrab=tr.codigoTrab AND re.codigoRto=rr.codigoRto
	AND r.fechaEntrada>'1/2/2017' AND r.fechaEntrada<'28/2/2017' AND v.tipoV='Camioneta'