CREATE DATABASE MECANICA
GO

USE MECANICA
GO

--USE master
--DROP DATABASE MECANICA

CREATE TABLE PROPIETARIOS (
	ciProp CHAR(10) PRIMARY KEY,
	nombreProp VARCHAR(20),
	apellidoProp VARCHAR(20),
	telefProp CHAR(10)
)

CREATE TABLE VEHICULOS (
	placaV CHAR(10) PRIMARY KEY,
	tipoV VARCHAR(10),
	colorV VARCHAR(10),
	a�oFabricV INT,
	ciProp CHAR(10),
	FOREIGN KEY(ciProp) REFERENCES PROPIETARIOS (ciProp)
)

CREATE TABLE REPARACIONES (
	codigoRep CHAR(3) PRIMARY KEY,
	fechaEntrada DATE,
	fechaSalida DATE,
	placaV CHAR(10),
	FOREIGN KEY(placaV) REFERENCES VEHICULOS (placaV)
)

CREATE TABLE TRABAJOS_REALIZADOS (
	codigoTrab CHAR(3) PRIMARY KEY,
	descripcionTrab VARCHAR(50),
	valorTrab MONEY
)

CREATE TABLE REPUESTOS (
	codigoRto CHAR(3) PRIMARY KEY,
	descripcionRto VARCHAR(50),
	valorRto MONEY
)

CREATE TABLE TRABAJO_REPARACION (
	codigoRep CHAR(3),
	codigoTrab CHAR(3),
	cantidadTrabajo INT,
	FOREIGN KEY(codigoRep) REFERENCES REPARACIONES (codigoRep),
	FOREIGN KEY(codigoTrab) REFERENCES TRABAJOS_REALIZADOS (codigoTrab)
)

CREATE TABLE REPUESTO_REPARACION (
	codigoRep CHAR(3),
	codigoRto CHAR(3),
	cantidadRep INT,
	PRIMARY KEY(codigoRep, codigoRto),
	FOREIGN KEY(codigoRep) REFERENCES REPARACIONES (codigoRep),
	FOREIGN KEY(codigoRto) REFERENCES REPUESTOS (codigoRto)
)

SELECT * FROM PROPIETARIOS

INSERT INTO PROPIETARIOS VALUES('1001396780', 'Ana', 'Corrales', '0983456780')
INSERT INTO PROPIETARIOS VALUES('1701396551', 'Juan', 'L�pez', '0998056112')

SELECT * FROM VEHICULOS

INSERT INTO VEHICULOS VALUES('IDC-3425', 'Cami�n', 'rojo', 1997, '1001396780')
INSERT INTO VEHICULOS VALUES('PPF-4511', 'Automovil', 'azul', 2017, '1001396780')
INSERT INTO VEHICULOS VALUES('ICD-0978', 'Automovil', 'blanco', 2010, '1001396780')

SELECT * FROM TRABAJOS_REALIZADOS

INSERT INTO TRABAJOS_REALIZADOS VALUES('T1', 'Cambio de aceite de motor', 10.0)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T2', 'Alineacion de llantas', 11.0)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T3', 'Limpieza de inyectores', 8.0)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T4', 'Cambio de filtros', 5.0)

SELECT * FROM REPUESTOS

INSERT INTO REPUESTOS VALUES('R1', 'Filtro de aceite', 8.0)
INSERT INTO REPUESTOS VALUES('R2', 'Filtro de aire', 15.0)
INSERT INTO REPUESTOS VALUES('R3', 'Llanta', 45.0)
INSERT INTO REPUESTOS VALUES('R4', 'Foco delantero', 5.0)

SELECT * FROM REPARACIONES

INSERT INTO REPARACIONES VALUES('R01', '01-12-2017', '01-13-2017', 'IDC-3425')
INSERT INTO REPARACIONES VALUES('R02', '01-12-17', '01-12-17', 'PPF-4511')
INSERT INTO REPARACIONES VALUES('R03', '01-15-17', '01-18-17', 'ICD-0978')
INSERT INTO REPARACIONES VALUES('R04', '01-17-17', '01-18-17', 'IDC-3425')

SELECT * FROM TRABAJO_REPARACION

INSERT INTO TRABAJO_REPARACION VALUES('R01', 'T1', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R01', 'T2', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R02', 'T1', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R02', 'T3', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R03', 'T1', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R04', 'T3', 1)

SELECT * FROM REPUESTO_REPARACION

INSERT INTO REPUESTO_REPARACION VALUES('R01', 'R1', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R01', 'R2', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R02', 'R1', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R03', 'R3', 4)
INSERT INTO REPUESTO_REPARACION VALUES('R04', 'R4', 1)

SELECT * FROM REPARACIONES

-- Para insertar un nuevo atributo en la tabla
ALTER TABLE REPARACIONES ADD valorTotalR MONEY
--ALTER TABLE REPARACIONES DROP COLUMN valorTotal

-- Para modificar el valorTotal
UPDATE REPARACIONES SET valorTotalR=45
WHERE codigoRep='R01'

UPDATE REPARACIONES SET valorTotalR=8+10+16
WHERE codigoRep='R02'

UPDATE REPARACIONES SET valorTotalR=4*45+10
WHERE codigoRep='R03'

UPDATE REPARACIONES SET valorTotalR=5+8
WHERE codigoRep='R04'

-- CONSULTAS B�SICAS CON SQL Y ALGEBRA RELACIONAL
-- Desplegar los datos de los veh�culos de color rojo
SELECT * FROM VEHICULOS WHERE colorV='rojo'

-- Desplegarlos datos de los veh�culos de color blanco cuyo a�o de fabricaci�n sea menor que el 2005
SELECT * FROM VEHICULOS WHERE colorV='blanco' AND a�oFabricV<2005

-- Desplegarlos datos de los veh�culos de color blanco o vehiculos cuyo a�o de fabricaci�n sea menor que el 2005
SELECT * FROM VEHICULOS WHERE colorV='blanco' OR a�oFabricV<2005

-- Desplegar la placa y el tipo  de los vehiculos.
SELECT placaV, tipoV FROM VEHICULOS

-- Por si...
--ALTER TABLE VEHICULOS
--   ALTER COLUMN a�oFabricV INT

-- Desplegar la placa de los vehiculos con a�o de fabricacion amyor que 2000
SELECT placaV FROM VEHICULOS WHERE a�oFabricV>2000

-- Desplegar el codigo y descripcion de los repuestos que cuestan menos de 10 d�lares
SELECT  codigoRto, descripcionRto FROM REPUESTOS WHERE valorRto<10

-- Desplegar las placas de los vehiculos que ingresaron al taller el 2 de enero de 2017

-- PRODUCTO CARTESIANO

--SELECT * FROM PROPIETARIOS CROSS JOIN VEHICULOS
--WHERE PROPIETARIOS.ciProp=VEHICULOS.ciProp

--SELECT * FROM PROPIETARIOS, VEHICULOS --Whats the difference?

SELECT * FROM PROPIETARIOS p INNER JOIN VEHICULOS v
ON p.ciProp=v.ciProp

-- Desplegar el nombre y apellido del propietario de automovil azul
SELECT nombreProp, apellidoProp FROM PROPIETARIOS p INNER JOIN VEHICULOS v
ON p.ciProp=v.ciProp WHERE tipoV='automovil' AND colorV='azul'

-- SELECT nombreProp, apellidoProp FROM PROPIETARIOS p, VEHICULOS v
--WHERE p.ciProp=v.ciProp AND tipoV='automovil' AND colorV='azul' -- Whats the difference?

--PRODUCTO NATURAL
--Desplegar las placas de los veh�culos que pusieron llantas nuevas en el taller
SELECT placaV FROM REPUESTOS r INNER JOIN REPUESTO_REPARACION rr ON r.codigoRto=rr.codigoRto
INNER JOIN REPARACIONES rp ON rr.codigoRep=rp.codigoRep WHERE descripcionRto='llanta'

--Desplegar los nombres y apellidos de los propietarios de vehiculos que limpiaron los inyectores
--en el taller
SELECT DISTINCT(p.nombreProp), p.apellidoProp FROM PROPIETARIOS p INNER JOIN VEHICULOS v
ON p.ciProp=v.ciProp INNER JOIN REPARACIONES r ON v.placaV=r.placaV INNER JOIN TRABAJO_REPARACION tr
ON r.codigoRep=tr.codigoRep INNER JOIN TRABAJOS_REALIZADOS tre ON tr.codigoTrab=tre.codigoTrab
WHERE tre.descripcionTrab='Limpieza de inyectores' -- Este es mio :)

SELECT DISTINCT(nombreProp), apellidoProp FROM PROPIETARIOS p, VEHICULOS v,
REPARACIONES r, TRABAJO_REPARACION tr, TRABAJOS_REALIZADOS tt
WHERE p.ciProp=v.ciProp AND v.placaV=r.placaV AND r.codigoRep=tr.codigoRep
AND tr.codigoTrab=tt.codigoTrab AND descripcionTrab='Limpieza de inyectores'

SELECT * FROM PROPIETARIOS WHERE 1=1

--Agregar en la tabla PROPIETARIOS la columna ciudadPropietario
ALTER TABLE PROPIETARIOS ADD ciudadPropietario VARCHAR(20)

UPDATE PROPIETARIOS SET ciudadPropietario='Quito'
WHERE ciProp='1001396780'

UPDATE PROPIETARIOS SET ciudadPropietario='Otavalo'
WHERE ciProp='1701396551'

--Ingresar 5 nuevos propietarios, dos de ellos tienen 1 vehiculos, uno tiene tres y 2 tienen 2 veh�vulos. 
--Las ciudades de los propietarios son: Ibarra, Otavalo y Quito.
SELECT * FROM PROPIETARIOS

INSERT INTO PROPIETARIOS VALUES('1001254451', 'Alma', 'Zuria', '0925478521', 'Ibarra')
INSERT INTO PROPIETARIOS VALUES('0325485269', 'Bryan', 'Omar', '0998885521', 'Otavalo')
INSERT INTO PROPIETARIOS VALUES('0214578596', 'Carlos', 'Kenai', '0985214523', 'Otavalo')
INSERT INTO PROPIETARIOS VALUES('1001254789', 'Daniel', 'Quinque', '0998123654', 'Ibarra')
INSERT INTO PROPIETARIOS VALUES('1201452456', 'Erick', 'Renato', '0998014785', 'Quito')

SELECT * FROM VEHICULOS

INSERT INTO VEHICULOS VALUES('PLC-6113', 'Automovil', 'blanco', 2015, '0325485269')
INSERT INTO VEHICULOS VALUES('BFG-9000', 'Automovil', 'negro', 2017, '0325485269')
INSERT INTO VEHICULOS VALUES('HJK-1548', 'Camioneta', 'azul', 2015, '0214578596')
INSERT INTO VEHICULOS VALUES('UTH-2013', 'Automovil', 'negro', 2001, '0214578596')
INSERT INTO VEHICULOS VALUES('SID-5645', 'Cami�n', 'rojo', 2012, '0214578596')
INSERT INTO VEHICULOS VALUES('FSA-6155', 'Camioneta', 'negro', 1998, '1001254789')
INSERT INTO VEHICULOS VALUES('FAS-7489', 'Automovil', 'rojo', 2014, '1001254789')
INSERT INTO VEHICULOS VALUES('NPI-0101', 'Camioneta', 'negro', 2012, '1201452456')
INSERT INTO VEHICULOS VALUES('NOP-1254', 'Cami�n', 'blanco', 2012, '1201452456')


--Agregar en la tabla VEHICULOS la columna marcaV. Agregar los veh�culos de los nuevos
--propietarios ingresados. En total deben ser 12 veh�culos en la tabla.
--Las marcas son: Kia, Nissan, Chevrolet y Toyota.
ALTER TABLE VEHICULOS ADD marcaV VARCHAR(20)

SELECT * FROM VEHICULOS

UPDATE VEHICULOS SET marcaV='Chevrolet'
WHERE placaV='BFG-9000'

UPDATE VEHICULOS SET marcaV='Nissan'
WHERE placaV='FAS-7489'

UPDATE VEHICULOS SET marcaV='Kia'
WHERE placaV='FSA-6155'

UPDATE VEHICULOS SET marcaV='Toyota'
WHERE placaV='HJK-1548'

UPDATE VEHICULOS SET marcaV='Chevrolet'
WHERE placaV='ICD-0978'

UPDATE VEHICULOS SET marcaV='Nissan'
WHERE placaV='IDC-3425'

UPDATE VEHICULOS SET marcaV='Kia'
WHERE placaV='NOP-1254'

UPDATE VEHICULOS SET marcaV='Toyota'
WHERE placaV='NPI-0101'

UPDATE VEHICULOS SET marcaV='Chevrolet'
WHERE placaV='PLC-6113'

UPDATE VEHICULOS SET marcaV='Nissan'
WHERE placaV='PPF-4511'

UPDATE VEHICULOS SET marcaV='Kia'
WHERE placaV='SID-5645'

UPDATE VEHICULOS SET marcaV='Toyota'
WHERE placaV='UTH-2013'


--En la tabla REPUESTOS ingresar: motor, costo 800 d�lares; transmisi�n,
--valor 300 d�lares; embrague, valor 200 d�lares
SELECT * FROM REPUESTOS

INSERT INTO REPUESTOS VALUES('R5', 'Motor', 800)
INSERT INTO REPUESTOS VALUES('R6', 'Transmisi�n', 300)
INSERT INTO REPUESTOS VALUES('R7', 'Embrague', 200)


--En la tabla TRABAJOS_REALIZADOS agregar: Desmontaje de motor, costo 100 d�lares;
--instalaci�n de motor nuevo, costo 300 d�lares, reemplazo de transmisi�n, 
--costo 200 d�lares; cambio de embrague, costo 100 d�lares.
SELECT * FROM TRABAJOS_REALIZADOS

INSERT INTO TRABAJOS_REALIZADOS VALUES('T5', 'Desmontaje de motor', 100)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T6', 'Instalaci�n de motor nuevo', 300)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T7', 'Reemplazo de transmisi�n', 200)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T8', 'Cambio de embrague', 100)


--Registrar el ingreso de todos los veh�culos al taller, en cada ingreso agregar 
--costos de repuestos y trabajos realizados, y calcular el valor total de la reparaci�n. 
--Al menos tres veh�culos deben tener dos ingresos al taller, y uno debe tener tres ingresos.
SELECT * FROM VEHICULOS
SELECT * FROM REPARACIONES
--
SELECT * FROM TRABAJO_REPARACION
SELECT * FROM TRABAJOS_REALIZADOS
--
SELECT * FROM REPUESTOS
SELECT * FROM REPUESTO_REPARACION


SELECT * FROM VEHICULOS
WHERE placaV!='IDC-3425' 
AND placaV!='PPF-4511'
AND placaV!='ICD-0978'
AND placaV!='IDC-3425'
AND placaV!='BFG-9000'
AND placaV!='FAS-7489'
AND placaV!='FSA-6155'
AND placaV!='HJK-1548'
AND placaV!='NOP-1254'
AND placaV!='NPI-0101'
AND placaV!='PLC-6113'
AND placaV!='SID-5645'

INSERT INTO REPARACIONES VALUES('R05', '2017-01-18', '2017-01-19', 'BFG-9000', 1000)
INSERT INTO REPARACIONES VALUES('R06', '2017-01-19', '2017-01-20', 'FAS-7489', 1000)
INSERT INTO REPARACIONES VALUES('R07', '2017-01-21', '2017-01-22', 'FAS-7489', 1000)
INSERT INTO REPARACIONES VALUES('R08', '2017-01-19', '2017-01-20', 'FSA-6155', 1000)
INSERT INTO REPARACIONES VALUES('R09', '2017-01-21', '2017-01-22', 'FSA-6155', 1000)
INSERT INTO REPARACIONES VALUES('R10', '2017-01-24', '2017-01-25', 'HJK-1548', 1000)
INSERT INTO REPARACIONES VALUES('R11', '2017-01-26', '2017-01-27', 'HJK-1548', 1000)
INSERT INTO REPARACIONES VALUES('R12', '2017-01-22', '2017-01-25', 'NOP-1254', 1000)
INSERT INTO REPARACIONES VALUES('R13', '2017-01-26', '2017-01-27', 'NOP-1254', 1000)
INSERT INTO REPARACIONES VALUES('R14', '2017-01-28', '2017-01-29', 'NOP-1254', 1000)
INSERT INTO REPARACIONES VALUES('R15', '2017-01-30', '2017-01-31', 'NPI-0101', 1000)
INSERT INTO REPARACIONES VALUES('R16', '2017-02-01', '2017-02-03', 'PLC-6113', 1000)
INSERT INTO REPARACIONES VALUES('R17', '2017-02-04', '2017-02-05', 'SID-5645', 1000)
INSERT INTO REPARACIONES VALUES('R18', '2017-02-07', '2017-02-10', 'UTH-2013', 1000)

SELECT * FROM REPARACIONES
SELECT * FROM TRABAJOS_REALIZADOS

INSERT INTO TRABAJO_REPARACION VALUES('R05', 'T1', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R06', 'T2', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R07', 'T3', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R08', 'T5', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R09', 'T6', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R10', 'T7', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R11', 'T8', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R12', 'T1', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R13', 'T2', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R14', 'T3', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R15', 'T4', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R16', 'T5', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R17', 'T6', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R18', 'T7', 2)

SELECT * FROM TRABAJO_REPARACION
--
SELECT * FROM REPUESTOS

INSERT INTO REPUESTO_REPARACION VALUES('R05', 'R1', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R06', 'R2', 2)
INSERT INTO REPUESTO_REPARACION VALUES('R07', 'R3', 3)
INSERT INTO REPUESTO_REPARACION VALUES('R08', 'R4', 4)
INSERT INTO REPUESTO_REPARACION VALUES('R09', 'R5', 5)
INSERT INTO REPUESTO_REPARACION VALUES('R10', 'R6', 2)
INSERT INTO REPUESTO_REPARACION VALUES('R11', 'R7', 4)
INSERT INTO REPUESTO_REPARACION VALUES('R12', 'R1', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R13', 'R2', 3)
INSERT INTO REPUESTO_REPARACION VALUES('R14', 'R3', 5)
INSERT INTO REPUESTO_REPARACION VALUES('R15', 'R4', 2)
INSERT INTO REPUESTO_REPARACION VALUES('R16', 'R5', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R17', 'R6', 2)
INSERT INTO REPUESTO_REPARACION VALUES('R18', 'R7', 1)

SELECT * FROM REPUESTO_REPARACION



UPDATE REPARACIONES SET valorTotalR=10+8
WHERE placaV='BFG-9000' AND codigoRep='R05'

UPDATE REPARACIONES SET valorTotalR=(11*2)+(15*2)
WHERE placaV='FAS-7489' AND codigoRep='R06'

UPDATE REPARACIONES SET valorTotalR=8+(3*45)
WHERE placaV='FAS-7489' AND codigoRep='R07'

UPDATE REPARACIONES SET valorTotalR=5+(4*5)
WHERE placaV='FSA-6155' AND codigoRep='R08'

UPDATE REPARACIONES SET valorTotalR=(2*300)+(5*800)
WHERE placaV='FSA-6155' AND codigoRep='R09'

UPDATE REPARACIONES SET valorTotalR=(2*200)+(2*300)
WHERE placaV='HJK-1548' AND codigoRep='R10'

UPDATE REPARACIONES SET valorTotalR=100+(4*200)
WHERE placaV='HJK-1548' AND codigoRep='R11'

UPDATE REPARACIONES SET valorTotalR=(2*10)+8
WHERE placaV='NOP-1254' AND codigoRep='R12'

UPDATE REPARACIONES SET valorTotalR=11+(3*15)
WHERE placaV='NOP-1254' AND codigoRep='R13'

UPDATE REPARACIONES SET valorTotalR=(2*8)+(5*45)
WHERE placaV='NOP-1254' AND codigoRep='R14'

UPDATE REPARACIONES SET valorTotalR=5+(2*5)
WHERE placaV='NPI-0101' AND codigoRep='R15'

UPDATE REPARACIONES SET valorTotalR=100+800
WHERE placaV='PLC-6113' AND codigoRep='R16'

UPDATE REPARACIONES SET valorTotalR=(2*300)+(2*300)
WHERE placaV='SID-5645' AND codigoRep='R17'

UPDATE REPARACIONES SET valorTotalR=(2*200)+(2*200)
WHERE placaV='UTH-2013' AND codigoRep='R18'


SELECT r.codigoRep, r.placaV, r.valorTotalR, trr.descripcionTrab, 
	tr.cantidadTrabajo, trr.valorTrab, rr.cantidadRep, re.valorRto
FROM REPARACIONES r, TRABAJO_REPARACION tr, REPUESTO_REPARACION rr, TRABAJOS_REALIZADOS trr, REPUESTOS re
WHERE r.codigoRep=tr.codigoRep AND rr.codigoRep=tr.codigoRep 
	AND trr.codigoTrab=tr.codigoTrab AND re.codigoRto=rr.codigoRto
--
AND placaV!='IDC-3425' 
AND placaV!='PPF-4511'
AND placaV!='ICD-0978'
AND placaV!='IDC-3425'
AND r.codigoRep!='R05'
AND r.codigoRep!='R06'
AND r.codigoRep!='R07'
AND r.codigoRep!='R08'
AND r.codigoRep!='R09'
AND r.codigoRep!='R10'
AND r.codigoRep!='R11'
AND r.codigoRep!='R12'
AND r.codigoRep!='R13'
AND r.codigoRep!='R14'
AND r.codigoRep!='R15'
AND r.codigoRep!='R16'
AND r.codigoRep!='R17'

--Desplegar los trabajos realizados y los repuestos junto a la placa y fecha de ingreso 
--de los vehiculos que ingresaron al taller en Enero del 2016 tipo camioneta.

SELECT trr.descripcionTrab, re.descripcionRto, r.placaV, r.fechaEntrada
FROM VEHICULOS v, REPARACIONES r, TRABAJO_REPARACION tr, REPUESTO_REPARACION rr, TRABAJOS_REALIZADOS trr, REPUESTOS re
WHERE v.placaV=r.placaV AND r.codigoRep=tr.codigoRep AND rr.codigoRep=tr.codigoRep 
	AND trr.codigoTrab=tr.codigoTrab AND re.codigoRto=rr.codigoRto
	AND r.fechaEntrada>'1/1/2017' AND r.fechaEntrada<'1/2/2017' AND v.tipoV='Camioneta'


--Funciones Matematicas
SELECT SQRT(14) --raiz cuadrada
SELECT PI() --Numero PI
SELECT  POWER(3,2) --Tres al cuadrado
SELECT  POWER(9, 0.3333) --Tres al cuadrado
SELECT SIN(PI()/2) --SENO
SELECT DEGREES(PI()) --Convertir radiantes a grados
SELECT COS(RADIANS(45))--Coseno de 45 grados
SELECT RADIANS(45.)--Convertir 45 grados a radianes
SELECT ROUND(3.61234,2)--Redondear a dos decimales
SELECT CEILING(3.2)--Subir al entero inmediato superior
SELECT FLOOR(3.2)--Bajar al entero imediato inferior
SELECT LOG(2.7172)--Logaritmo Natural
SELECT LOG10(10)--Logaritmo Base 10

--Matematica
DECLARE @x FLOAT;
SET @x = 1;
SELECT @x AS x, (POWER(@x, 3) - ((2 * @x) * SQRT(5 - LOG(@x)))) / (12 * TAN(@x/3)) + POWER(@x, 3/2) AS y
SET @x = 2;
SELECT @x AS x, (POWER(@x, 3) - ((2 * @x) * SQRT(5 - LOG(@x)))) / (12 * TAN(@x/3)) + POWER(@x, 3/2) AS y
SET @x = 3;
SELECT @x AS x, (POWER(@x, 3) - ((2 * @x) * SQRT(5 - LOG(@x)))) / (12 * TAN(@x/3)) + POWER(@x, 3/2) AS y

--FUNCIONES DE CARACTERES
SELECT SUBSTRING('universidad tecnica del norte', 5, 8) --Extraer una cadena que inicia en la posicion 5
--y su longitud es 8
SELECT UPPER('universidad tecnica del norte') --Convierte a mayusculas una cadena
SELECT LOWER('MINUSCULAS') --Convierte a minusculas
SELECT LEFT('MINUSCULAS', 4) --Extrae 4 caracteres desde la izquierda
SELECT RIGHT('MINUSCULAS', 6) --Extrae 6 caracteres desde la derecha
SELECT LEN('universidad tecnica del norte') --Cuenta en nro de caracteres de una cadena
SELECT STR(3.245, 7, 2) --Convierte un n�mero decimal en una cadena de 7 espacios con 2 decimales
SELECT STUFF('universidad tecnica del norte',5,4,'XXX') --Reemplaza una parte de un cadena por otra
SELECT SPACE(7) --Inserta 7 espacios
SELECT CHAR(92) --Simbolo del 92 ASCII

--Desplegar los datos de los propietarios cuyo nombre inicie con vocal
SELECT * FROM PROPIETARIOS WHERE LEFT(nombreProp,1)='A' OR LEFT(nombreProp,1)='E' OR
LEFT(nombreProp,1)='I' OR LEFT(nombreProp,1)='O' OR LEFT(nombreProp,1)='U'

SELECT * FROM PROPIETARIOS
--Desplegar los datos de los propietarios cuyo nombre termine en vocal y cuyo apellido tenga mas de 5 estrellas
SELECT * FROM PROPIETARIOS WHERE (RIGHT(nombreProp,1)='A' OR RIGHT(nombreProp,1)='E' OR RIGHT(nombreProp,1)='I'
OR RIGHT(nombreProp,1)='O' OR RIGHT(nombreProp,1)='U') 

