select datename (mm,fechaEntrada)as ' dia de ingreso' from REPARACIONES
select dateadd(mm,2,getdate())-- agrega 2 meses a la fecha actual
select datediff(dd,'12-05-2005','30-06-2008')
select datediff(yy,getdate(),'01-01-2000')-- Diferencias en dias entre dos fechas

-- DIAS EN EL TALLER select placaV, datediff (dd,fechaEntrada, fechaSalida) as 'Dias en el Taller' from REPARACIONES

SELECT 'EL VEHICULO DE PLACAS: '+ placaV+' estuvo'+ str(datediff(dd,fechaEntrada,fechaSalida))+
' dias en reparaciones ' as 'INFORMACI�N DE VEHICULOS' from REPARACIONES 
SELECT YEAR(GETDATE())
SELECT MONTH(fechaSalida) from REPARACIONES
SELECT DAY (fechaEntrada) from REPARACIONES
-- EJERCICIO
-- CREAR LA FRASE: EL VEHICULO DE LA PLACA __________ INGRES� AL TALLER EL 21 DE ENERO DE 2017
-- PARA TODOS LOS VEHICULOS

SELECT 'EL VEHICULO DE LA PLACA: '+placaV+' ingres� al taller el '+STR(DAY (fechaEntrada))+
' de '+datename(mm,fechaEntrada)+' de '+datename(yy,fechaEntrada) as 'Ejercicio' FROM REPARACIONES

select convert (char(20), GETDATE(),100)-- transforma la fecha actual a un char(20)
-- con el formato prestablecido 100
select convert (char(20), getdate(),110)
select convert(char(20),getdate(),104)

select REPLICATE(' MA�ANA EXAMEN', 10)
select 'El due�o del vehiculo con placas '+space(5)+placaV+space(5)+'es '+nombreProp+space(2)+apellidoProp
from PROPIETARIOS P inner join VEHICULOS V on p.ciProp=v.ciProp
-- ejercicio 2
-- crear la frase: el vehiculo de placas ____ ingres� al taller el ____ y salio el ________ 
-- su reparaci�n costo $______
Select 'El vehiculo de placas'+space(5)+R.placaV+space(5)+'ingres� al taller el'+
space(2)+STR(DAY (fechaEntrada))+' de '+datename(mm,fechaEntrada)+' de '+datename(yy,fechaEntrada)+space(2)+
'y salio el'+ SPACE(2)+STR(DAY (fechaSalida))+' de '+datename(mm,fechaSalida)+' de '+datename(yy,fechaSalida)+
space(5)+'su reparacion costo $'+STR(R.valorTotalR) from REPARACIONES R 
-- ELIMINAR LOS ESPACIOS QUE ESTAN DEMAS:
SELECT LTRIM('       HOLA')
-- EJERCICIO 3
--CREAR LA FRASE: LA [TIPO VEHICULO] COLOR__________NECESIT� [NRO RESPUESTOS] [DESCRIPCION RTO] 
-- EN LA REPARACI�N [ C�DIGO REPARACION]
SELECT 'EL '+V.tipoV+SPACE(5)+V.colorV+space(2)+'necesit�'+STR(repre.cantidadRep)+space(5)+re.descripcionRto+
'en la reparaci�n - '+repre.codigoRep from VEHICULOS v, REPUESTO_REPARACION repre, REPARACIONES REP, REPUESTOS re where 
v.placaV=REP.placaV  AND RE.codigoRto=repre.codigoRto
SELECT * FROM VEHICULOS
SELECT * FROM REPARACIONES
SELECT * FROM REPUESTOS
SELECT * FROM REPUESTO_REPARACION


-- USO DE WHERE 
--Desplegar los datos de los vehiculos que ingresaron al taller en enero del 2017
select  v.* from VEHICULOS  v inner join REPARACIONES r or v.placaV = r.placaV where MONTH(fechaEntrada)=1

--Desplegar los nombres y apellidos de los propietarios de los propietarios cuyos nombres inicien con vocal 
--y terminen con consonante
select p.nombreProp , p.apellidoProp  from PROPIETARIOS p where 
left(p.nombreProp,1)= 'A' or --El uno quiere decir que coge la porimera letra de una palabra
left(p.nombreProp,1)= 'E' or
left(p.nombreProp,1)= 'I' or
left(p.nombreProp,1)= 'O' or
left(p.nombreProp,1)= 'U' 

select p.nombreProp , p.apellidoProp  from PROPIETARIOS p where nombreProp like '[AEIOU]%'
and nombreProp not like '%[AEIOU]'
 
 insert into PROPIETARIOS vaplues('1700234546','Andr�s','Contreras','0987654321','Quito')

-- Desplegar nombre y apellidos de propietarios que viven en Ibarra o en Quito 
select nombreProp , apellidoProp , ciudadPropietario from PROPIETARIOS 
where ciudadPropietario in ('Quito','Ibarra')

--Quye no viven ni en Quito ni en Ibarra.....
select nombreProp , apellidoProp , ciudadPropietario from PROPIETARIOS 
where ciudadPropietario not in ('Quito','Ibarra')
--Desplegar los nombres y apellidos de propietarios que tienen dentro de su apellido 
-- la cadena 'don'
-- like = hace buscar coincidencias 
select nombreProp , apellidoProp , ciudadPropietario from PROPIETARIOS 
where apellidoProp like '%don%'
--desplegar los datos de los vehiculos que ingresaro al taller del 15 al 31 de enero del 2017
-- v.* = selecciona todas las columnas d una tabla
select v.*,fechaEntrada from VEHICULOS v inner join REPARACIIONES r on v.placaV = r.placaV where 
lfechaEntrada between
'15-01-17' and '31-01-2017'

-- Los datos de los vehiculos que no ingresaron en ese intervalo
select v.*,fechaEntrada from VEHICULOS v inner join REPARACIIONES r on v.placaV = r.placaV where 
fechaEntrada not wbetween
'15-01-17' and '31-01-2017'
--Desplegar los datos de los vehiculos con placa de Imbabura cuyos due�os viven en Quito
--y estuvieron mas de un dia en el taller 
select * from VEHICULOS v where 
left(v.placaV,1)= 'I' and 

  