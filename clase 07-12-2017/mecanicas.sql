create database Mecanicass
go
use Mecanicass
go
CREATE TABLE PROPIETARIOS (
	ciProp CHAR(10) PRIMARY KEY,
	nombreProp VARCHAR(20),
	apellidoProp VARCHAR(20),
	telefProp CHAR(10)
)

CREATE TABLE VEHICULOS (
	placaV CHAR(10) PRIMARY KEY,
	tipoV VARCHAR(10),
	colorV VARCHAR(10),
	a�oFabricV INT,
	ciProp CHAR(10),
	FOREIGN KEY(ciProp) REFERENCES PROPIETARIOS (ciProp)
)

CREATE TABLE REPARACIONES (
	codigoRep CHAR(3) PRIMARY KEY,
	fechaEntrada DATE,
	fechaSalida DATE,
	placaV CHAR(10),
	FOREIGN KEY(placaV) REFERENCES VEHICULOS (placaV)
)

CREATE TABLE TRABAJOS_REALIZADOS (
	codigoTrab CHAR(3) PRIMARY KEY,
	descripcionTrab VARCHAR(50),
	valorTrab MONEY
)

CREATE TABLE REPUESTOS (
	codigoRto CHAR(3) PRIMARY KEY,
	descripcionRto VARCHAR(50),
	valorRto MONEY
)

CREATE TABLE TRABAJO_REPARACION (
	codigoRep CHAR(3),
	codigoTrab CHAR(3),
	cantidadTrabajo INT,
	FOREIGN KEY(codigoRep) REFERENCES REPARACIONES (codigoRep),
	FOREIGN KEY(codigoTrab) REFERENCES TRABAJOS_REALIZADOS (codigoTrab)
)

CREATE TABLE REPUESTO_REPARACION (
	codigoRep CHAR(3),
	codigoRto CHAR(3),
	cantidadRep INT,
	PRIMARY KEY(codigoRep, codigoRto),
	FOREIGN KEY(codigoRep) REFERENCES REPARACIONES (codigoRep),
	FOREIGN KEY(codigoRto) REFERENCES REPUESTOS (codigoRto)
)
------------
SELECT * FROM PROPIETARIOS

INSERT INTO PROPIETARIOS VALUES('1001396780', 'Ana', 'Corrales', '0983456780')
INSERT INTO PROPIETARIOS VALUES('1701396551', 'Juan', 'L�pez', '0998056112')

SELECT * FROM VEHICULOS

INSERT INTO VEHICULOS VALUES('IDC-3425', 'Cami�n', 'rojo', 1997, '1001396780')
INSERT INTO VEHICULOS VALUES('PPF-4511', 'Automovil', 'azul', 2017, '1001396780')
INSERT INTO VEHICULOS VALUES('ICD-0978', 'Automovil', 'blanco', 2010, '1001396780')

SELECT * FROM TRABAJOS_REALIZADOS

INSERT INTO TRABAJOS_REALIZADOS VALUES('T1', 'Cambio de aceite de motor', 10.0)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T2', 'Alineacion de llantas', 11.0)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T3', 'Limpieza de inyectores', 8.0)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T4', 'Cambio de filtros', 5.0)

SELECT * FROM REPUESTOS

INSERT INTO REPUESTOS VALUES('R1', 'Filtro de aceite', 8.0)
INSERT INTO REPUESTOS VALUES('R2', 'Filtro de aire', 15.0)
INSERT INTO REPUESTOS VALUES('R3', 'Llanta', 45.0)
INSERT INTO REPUESTOS VALUES('R4', 'Foco delantero', 5.0)

SELECT * FROM REPARACIONES

INSERT INTO REPARACIONES VALUES('R01', '2017-01-01', '2017-01-13', 'IDC-3425')
INSERT INTO REPARACIONES VALUES('R02', '2017-01-01', '2017-01-12', 'PPF-4511')
INSERT INTO REPARACIONES VALUES('R03', '2017-01-01', '2017-01-18', 'ICD-0978')
INSERT INTO REPARACIONES VALUES('R04', '2017-01-01', '2017-01-01', 'IDC-3425')

SELECT * FROM TRABAJO_REPARACION

INSERT INTO TRABAJO_REPARACION VALUES('R01', 'T1', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R01', 'T2', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R02', 'T1', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R02', 'T3', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R03', 'T1', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R04', 'T3', 1)

SELECT * FROM REPUESTO_REPARACION

INSERT INTO REPUESTO_REPARACION VALUES('R01', 'R1', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R01', 'R2', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R02', 'R1', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R03', 'R3', 4)
INSERT INTO REPUESTO_REPARACION VALUES('R04', 'R4', 1)

SELECT * FROM REPARACIONES

-- Para INSERTAR un nuevo atributo en la tabla
ALTER TABLE REPARACIONES ADD valorTotalR MONEY
--ALTER TABLE REPARACIONES DROP COLUMN valorTotal

-- Para modificar el valorTotal
UPDATE REPARACIONES SET valorTotalR=45
WHERE codigoRep='R01'

UPDATE REPARACIONES SET valorTotalR=8+10+16
WHERE codigoRep='R02'

UPDATE REPARACIONES SET valorTotalR=4*45+10
WHERE codigoRep='R03'

UPDATE REPARACIONES SET valorTotalR=5+8
WHERE codigoRep='R04'
-- CONSULTAS B�SICAS CON SQL Y ALGEBRA RELACIONAL
-- 1 Desplegar los datos de los veh�culos de color rojo
SELECT * FROM VEHICULOS WHERE colorV='rojo'
-- Desplegarlos datos de los veh�culos de color blanco cuyo a�o de fabricaci�n sea menor que el 2005
SELECT * FROM VEHICULOS WHERE colorV='blanco' AND a�oFabricV<2005

-- 2 Desplegarlos datos de los veh�culos de color blanco o vehiculos cuyo a�o de fabricaci�n sea menor que el 2005
SELECT * FROM VEHICULOS WHERE colorV='blanco' OR a�oFabricV<2005

-- 3 Desplegar la placa y el tipo  de los vehiculos.
SELECT placaV, tipoV FROM VEHICULOS

-- Por si...
-- ALTER TABLE VEHICULOS
--   ALTER COLUMN a�oFabricV INT

-- 4 Desplegar la placa de los vehiculos con a�o de fabricacion amyor que 2000
SELECT placaV FROM VEHICULOS WHERE a�oFabricV>2000

-- 5 Desplegar el codigo y descripcion de los repuestos que cuestan menos de 10 d�lares
SELECT  codigoRto, descripcionRto FROM REPUESTOS WHERE valorRto<10

-- 6 Desplegar las placas de los vehiculos que ingresaron al taller el 2 de enero de 2017

-- PRODUCTO CARTESIANO

--SELECT * FROM PROPIETARIOS CROSS JOIN VEHICULOS
--WHERE PROPIETARIOS.ciProp=VEHICULOS.ciProp

--SELECT * FROM PROPIETARIOS, VEHICULOS --Whats the difference?

SELECT * FROM PROPIETARIOS p INNER JOIN VEHICULOS v
ON p.ciProp=v.ciProp

-- 7 Desplegar el nombre y apellido del propietario de automovil azul
SELECT nombreProp, apellidoProp FROM PROPIETARIOS p INNER JOIN VEHICULOS v
ON p.ciProp=v.ciProp WHERE tipoV='automovil' AND colorV='azul'

-- 8 SELECT nombreProp, apellidoProp FROM PROPIETARIOS p, VEHICULOS v
--WHERE p.ciProp=v.ciProp AND tipoV='automovil' AND colorV='azul' -- Whats the difference?

-- PRODUCTO NATURAL
-- Desplegar las placas de los veh�culos que pusieron llantas nuevas en el taller
SELECT placaV FROM REPUESTOS r INNER JOIN REPUESTO_REPARACION rr ON r.codigoRto=rr.codigoRto
INNER JOIN REPARACIONES rp ON rr.codigoRep=rp.codigoRep WHERE descripcionRto='llanta'

-- Desplegar los nombres y apellidos de los propietarios de vehiculos que limpiaron los inyectores
-- en el taller
SELECT DISTINCT(p.nombreProp), p.apellidoProp FROM PROPIETARIOS p INNER JOIN VEHICULOS v
ON p.ciProp=v.ciProp INNER JOIN REPARACIONES r ON v.placaV=r.placaV INNER JOIN TRABAJO_REPARACION tr
ON r.codigoRep=tr.codigoRep INNER JOIN TRABAJOS_REALIZADOS tre ON tr.codigoTrab=tre.codigoTrab
WHERE tre.descripcionTrab='Limpieza de inyectores' -- Este es mio :)

SELECT DISTINCT(nombreProp), apellidoProp FROM PROPIETARIOS p, VEHICULOS v,
REPARACIONES r, TRABAJO_REPARACION tr, TRABAJOS_REALIZADOS tt
WHERE p.ciProp=v.ciProp AND v.placaV=r.placaV AND r.codigoRep=tr.codigoRep
AND tr.codigoTrab=tt.codigoTrab AND descripcionTrab='Limpieza de inyectores'
-----------------------------------------------------------------------------------------
---- --1) Agregar en la tabla PROPIETARIOS la columna ciudadPropietario
ALTER TABLE PROPIETARIOS ADD ciudadPropietario VARCHAR(20)
-- agrego a los que no tenian
select * from PROPIETARIOS
UPDATE PROPIETARIOS SET ciudadPropietario='Quito'
WHERE ciProp='1001396780'

UPDATE PROPIETARIOS SET ciudadPropietario='Otavalo'
WHERE ciProp='1701396551'
-- --2 ) Ingresar 5 nuevos propietarios, dos de ellos tienen 1 vehiculos, uno tiene tres y 2 tienen 2 veh�vulos. 
--Las ciudades de los propietarios son: Ibarra, Otavalo y Quito.
INSERT INTO PROPIETARIOS VALUES('1001111111', 'Edwin', 'Cachiguango', '0994501275', 'Ibarra')
INSERT INTO PROPIETARIOS VALUES('1001111122', 'Bryan', 'Mayers', '099450000', 'Otavalo')
INSERT INTO PROPIETARIOS VALUES('1001111133', 'Johatan', 'Carlosama', '0985111111', 'Otavalo')
INSERT INTO PROPIETARIOS VALUES('1001111144', 'Rosa', 'Yamberla', '0998122223', 'Ibarra')
INSERT INTO PROPIETARIOS VALUES('1001111155', 'Gustavo', 'Hernandez', '0924689120', 'Quito')
select * from PROPIETARIOS
SELECT * FROM VEHICULOS
 
-- 3) --Agregar en la tabla VEHICULOS la columna marcaV. Agregar los veh�culos de los nuevos
--propietarios ingresados. En total deben ser 12 veh�culos en la tabla.
--Las marcas son: Kia, Nissan, Chevrolet y Toyota.
ALTER TABLE VEHICULOS ADD marcaV VARCHAR(20)
SELECT * FROM VEHICULOS
-- modificar los que ya tenia
UPDATE VEHICULOS SET marcaV='Chevrolet'
WHERE placaV='ICD-0978'
UPDATE VEHICULOS SET marcaV='Chevrolet'
WHERE placaV='IDC-3425'
UPDATE VEHICULOS SET marcaV='Kia'
WHERE placaV='PPF-4511'
--- ingresar los nuevos vehiculos
INSERT INTO VEHICULOS VALUES('PBC-O300', 'Automovil', 'blanco', 2010, '1001111111','Kia')
INSERT INTO VEHICULOS VALUES('WWW-1200', 'Automovil', 'azul', 2011, '1001111133','Chevrolet')
INSERT INTO VEHICULOS VALUES('ICH-1994', 'Camioneta', 'azul', 2012, '1001111155','Nissan')
INSERT INTO VEHICULOS VALUES('PDC-2013', 'Automovil', 'rojo', 2013, '1001111111','Nissan')
INSERT INTO VEHICULOS VALUES('ORG-2015', 'Camioneta', 'negro', 2015, '1001111144','Toyota')
INSERT INTO VEHICULOS VALUES('COM-7400', 'Automovil', 'rojo', 2016, '1001111122','Toyota')
INSERT INTO VEHICULOS VALUES('ECU-9111', 'Automovil', 'gris', 2017, '1001111122','Kia')
--4) En la tabla REPUESTOS ingresar: motor, costo 800 d�lares; transmisi�n,
--valor 300 d�lares; embrague, valor 200 d�lares
SELECT * FROM REPUESTOS

INSERT INTO REPUESTOS VALUES('R5', 'Motor', 800)
INSERT INTO REPUESTOS VALUES('R6', 'Transmisi�n', 300)
INSERT INTO REPUESTOS VALUES('R7', 'Embrague', 200)
----5) En la tabla TRABAJOS_REALIZADOS agregar: Desmontaje de motor, costo 100 d�lares;
--instalaci�n de motor nuevo, costo 300 d�lares, reemplazo de transmisi�n, 
--costo 200 d�lares; cambio de embrague, costo 100 d�lares.
SELECT * FROM TRABAJOS_REALIZADOS
INSERT INTO TRABAJOS_REALIZADOS VALUES('T5', 'Desmontaje de motor', 100)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T6', 'Instalaci�n de motor nuevo', 300)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T7', 'Reemplazo de transmisi�n', 200)
INSERT INTO TRABAJOS_REALIZADOS VALUES('T8', 'Cambio de embrague', 100)
---- 6) Registrar el ingreso de todos los veh�culos al taller, en cada ingreso agregar 
--costos de repuestos y trabajos realizados, y calcular el valor total de la reparaci�n. 
-- Al menos tres veh�culos deben tener dos ingresos al taller, y uno debe tener tres ingresos.
select * from VEHICULOS 
INSERT INTO REPARACIONES VALUES('R05', '2017-01-18', '2017-01-19', 'COM-7400', 1000)
INSERT INTO REPARACIONES VALUES('R06', '2017-01-19', '2017-01-20', 'ECU-9111', 2500)
INSERT INTO REPARACIONES VALUES('R07', '2017-01-21', '2017-01-22', 'ICD-0978', 100)
INSERT INTO REPARACIONES VALUES('R08', '2017-01-19', '2017-01-20', 'ICH-1994', 1000)
INSERT INTO REPARACIONES VALUES('R09', '2017-01-21', '2017-01-22', 'IDC-3425', 1405)
INSERT INTO REPARACIONES VALUES('R10', '2017-01-24', '2017-01-25', 'ORG-2015', 1000)
INSERT INTO REPARACIONES VALUES('R11', '2017-01-26', '2017-01-27', 'PBC-O300', 800)
INSERT INTO REPARACIONES VALUES('R12', '2017-01-22', '2017-01-25', 'PDC-2013', 1000)
INSERT INTO REPARACIONES VALUES('R13', '2017-01-26', '2017-01-27', 'PPF-4511', 1002)
INSERT INTO REPARACIONES VALUES('R14', '2017-01-28', '2017-01-29', 'WWW-1200', 500)
INSERT INTO REPARACIONES VALUES('R15', '2017-01-30', '2017-01-31', 'WWW-1200', 1003)
INSERT INTO REPARACIONES VALUES('R16', '2017-02-01', '2017-02-03', 'ECU-9111', 1006)
INSERT INTO REPARACIONES VALUES('R17', '2017-02-04', '2017-02-05', 'COM-7400', 300)
INSERT INTO REPARACIONES VALUES('R18', '2017-02-07', '2017-02-10', 'ICH-1994', 1008)
-- 
SELECT * FROM REPARACIONES
SELECT * FROM TRABAJOS_REALIZADOS
SELECT * FROM TRABAJO_REPARACION
INSERT INTO TRABAJO_REPARACION VALUES('R05', 'T1', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R06', 'T2', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R07', 'T3', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R08', 'T5', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R09', 'T6', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R10', 'T7', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R11', 'T8', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R12', 'T1', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R13', 'T2', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R14', 'T3', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R15', 'T4', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R16', 'T5', 1)
INSERT INTO TRABAJO_REPARACION VALUES('R17', 'T6', 2)
INSERT INTO TRABAJO_REPARACION VALUES('R18', 'T7', 2)

SELECT * FROM REPARACIONES
SELECT * FROM TRABAJOS_REALIZADOS

INSERT INTO REPUESTO_REPARACION VALUES('R05', 'R1', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R06', 'R2', 2)
INSERT INTO REPUESTO_REPARACION VALUES('R07', 'R3', 3)
INSERT INTO REPUESTO_REPARACION VALUES('R08', 'R4', 4)
INSERT INTO REPUESTO_REPARACION VALUES('R09', 'R5', 5)
INSERT INTO REPUESTO_REPARACION VALUES('R10', 'R6', 2)
INSERT INTO REPUESTO_REPARACION VALUES('R11', 'R7', 4)
INSERT INTO REPUESTO_REPARACION VALUES('R12', 'R1', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R13', 'R2', 3)
INSERT INTO REPUESTO_REPARACION VALUES('R14', 'R3', 5)
INSERT INTO REPUESTO_REPARACION VALUES('R15', 'R4', 2)
INSERT INTO REPUESTO_REPARACION VALUES('R16', 'R5', 1)
INSERT INTO REPUESTO_REPARACION VALUES('R17', 'R6', 2)
INSERT INTO REPUESTO_REPARACION VALUES('R18', 'R7', 1)
SELECT r.codigoRep, r.placaV, r.valorTotalR, trr.descripcionTrab, 
	tr.cantidadTrabajo, trr.valorTrab, rr.cantidadRep, re.valorRto
FROM REPARACIONES r, TRABAJO_REPARACION tr, REPUESTO_REPARACION rr, TRABAJOS_REALIZADOS trr, REPUESTOS re
WHERE r.codigoRep=tr.codigoRep AND rr.codigoRep=tr.codigoRep 
	AND trr.codigoTrab=tr.codigoTrab AND re.codigoRto=rr.codigoRto
	-------------------------
	select datename (mm,fechaEntrada)as ' dia de ingreso' from REPARACIONES
select dateadd(mm,2,getdate())-- agrega 2 meses a la fecha actual
select datediff(dd,'12-05-2005','30-06-2008')
select datediff(yy,getdate(),'01-01-2000')-- Diferencias en dias entre dos fechas
-- DIAS EN EL TALLERselect placaV, datediff (dd,fechaEntrada, fechaSalida) as 'Dias en el Taller' from REPARACIONES

SELECT 'EL VEHICULO DE PLACAS: '+ placaV+' estuvo'+ str(datediff(dd,fechaEntrada,fechaSalida))+
' dias en reparaciones ' as 'INFORMACI�N DE VEHICULOS' from REPARACIONES 
SELECT YEAR(GETDATE())
SELECT MONTH(fechaSalida) from REPARACIONES
SELECT DAY (fechaEntrada) from REPARACIONES
-- EJERCICIO
-- CREAR LA FRASE: EL VEHICULO DE LA PLACA __________ INGRES� AL TALLER EL 21 DE ENERO DE 2017
-- PARA TODOS LOS VEHICULOS

SELECT 'EL VEHICULO DE LA PLACA: '+placaV+' ingres� al taller el '+STR(DAY (fechaEntrada))+
' de '+datename(mm,fechaEntrada)+' de '+datename(yy,fechaEntrada) as 'Ejercicio' FROM REPARACIONES