-- Restricciones de integridad
 -- son herramientas para agregar caracteristicas particulares que deben tener 
 --los datos que ingresan a una base de para cumplir las reglas del negocio y su ingreso sea controlado automaticamente
 -- por el sistema

 create database Empresa
 go
 use Empresa
 go
  create table EMPLEADOS
  (
  codigoEmp int,
  nombre varchar(50),
  apellido varchar(50),
  fechaingreso date,
  sueldo money
  )
  Select * from EMPLEADOS
   
   INSERT INTO EMPLEADOS values(1,'Carlos','Fuentes','12-01-90','387')
   INSERT INTO EMPLEADOS values(5,'Carlos','Fuentes','12-01-90','360.50')
   INSERT INTO EMPLEADOS values(6,'Juan','Ayala','20-02-60',6)
   INSERT INTO EMPLEADOS values(3,'Alicia','Benitez','20-02-92',500)
   INSERT INTO EMPLEADOS values(null,'Pedro','Vaca','01-01-2500',800)
   INSERT INTO EMPLEADOS values(2,'Lucas','Bucaram','13-01-19',-560)

   drop table Empleados
-- corregimos y agregamos restricciones
create table EMPLEADOS
  (
  codigoEmp int primary key,
  nombre varchar(50) not null,
  apellido varchar(50)not null,
  fechaingreso date default '01-01-2000',
  sueldo money check (sueldo>386)
  ) 
  Select * from EMPLEADOS
   INSERT INTO EMPLEADOS values(1,'Carlos','Fuentes','12-01-90',387)
   INSERT INTO EMPLEADOS values(5,'Carlos','Fuentes','12-01-90',388)
   INSERT INTO EMPLEADOS values(6,'Juan','Ayala','20-02-60',400)
   INSERT INTO EMPLEADOS values(3,'Alicia','Benitez','20-02-92',500)
   INSERT INTO EMPLEADOS values(10,'Pedro','Vaca','01-01-2500',800)
   INSERT INTO EMPLEADOS values(2,'Lucas','Bucaram','13-01-19',560)
  -- INSERT INTO EMPLEADOS (codigoEmp, nombre, apellido, fechaingreso, sueldo)
-- otra forma de hacer estricciones
drop table EMPLEADOS
create table EMPLEADOS
  (
  codigoEmp int primary key identity,
  nombre varchar(50) not null unique,
  apellido varchar(50)not null,
  fechaingreso date default '03-01-2000' 
  check(Datename(dw, fechaingreso)<>'S�bado' and Datename(dw, fechaingreso)<>'Domingo' and fechaingreso<=getdate()),
  sueldo money check (sueldo>=386 and sueldo<=2000)
  ) 

  Select * from EMPLEADOS
   INSERT INTO EMPLEADOS values('Carlos','Fuentes','12-01-90',387)
   INSERT INTO EMPLEADOS values('Luis','Fuentes','12-01-2010',388)
   INSERT INTO EMPLEADOS values('Juan','Ayala','04-01-2018',400)
   INSERT INTO EMPLEADOS values('Alicia','Benitez','28-02-92',500)
   INSERT INTO EMPLEADOS values('Pedro','Vaca','11-01-2018',800)
   INSERT INTO EMPLEADOS values('Lucas','Bucaram','01-01-18',560)
  
--EJERICIO EN CLASE
-- CREAR LA TABLA DEPARTAMENTOS CON LAS SIGUIENTES COLUMNAS
-- codigoDto es entero autoincrementable inicia en 100 e incrementa de 2 en 2 identity(100,2) 100 es empieza y 2 se incrementa de 2 en 2
-- nombreDpto puede ser solo T�cnico, Financiero o Legal
-- NroEmpleados minimo 3
DROP TABLE DEPARTAMENTOS
create table DEPARTAMENTOS
(
codigoDto int primary key  identity(100,2),
nombreDto varchar (20) not null unique check(nombreDto='T�cnico' or nombreDto='Legal' or nombreDto='Financiero'),
-- nombreDto varchar (20) not null unique check(nombreDto in('T�nico','Legal','Financiero')),
NroEmpleados int check (NroEmpleados>=3),
) 

select * from Departamentos
 INSERT INTO DEPARTAMENTOS values('T�cnico',3)
 INSERT INTO DEPARTAMENTOS values('Legal',3)
 INSERT INTO DEPARTAMENTOS values('Financiero',3)
 -- 
 drop table Empleados
 create table EMPLEADOS
  (
  codigoEmp int primary key identity,
  codigoDto int references DEPARTAMENTOS(codigoDto) check(codigoDto>=100), 
  nombre varchar(50) not null unique,
  apellido varchar(50)not null,
  fechaingreso date default '03-01-2000' 
  check(Datename(dw, fechaingreso)<>'S�bado' and Datename(dw, fechaingreso)<>'Domingo' and fechaingreso<=getdate()),
  sueldo money check (sueldo>=386 and sueldo<=2000),
  unique(nombre, apellido)
  ) 
  Select * from EMPLEADOS
   INSERT INTO EMPLEADOS values(100,'Carlos','Tontaquimba','11-01-2018',500)